const charIndexString = (str) => {
    const alphabet = [];
    // letters from a to z
    for (let i = 97; i <= 122; i++) {
        alphabet.push(String.fromCharCode(i));
    }

    const charIndex = {};

    for (const letter in alphabet) {
        charIndex[alphabet[letter]] = parseInt(letter) + 1;
    }

    let letterIndicesInAlphabet = '';

    for (const letter of str) {
        letterIndicesInAlphabet += charIndex[letter];
    }

    return letterIndicesInAlphabet;
};

console.log(charIndexString('bead')); // prints "2514"
console.log(charIndexString('rose')); // prints "1815195"
