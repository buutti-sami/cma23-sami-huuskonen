function sortNumberArray(arrToSort) {
    let swapped;

    do {
        swapped = false;
        for (let i = 0; i < arrToSort.length - 1; i++) {
            if (arrToSort[i] > arrToSort[i + 1]) {
                const temp = arrToSort[i];
                arrToSort[i] = arrToSort[i + 1];
                arrToSort[i + 1] = temp;
                swapped = true;
            }
        }
    } while (swapped);

    return arrToSort;
}

const array = [4, 19, 7, 1, 9, 22, 6, 13];
sortNumberArray(array);
console.log(array); // prints [ 1, 4, 6, 7, 9, 13, 19, 22 ]
