function isPalindrome(str) {
    const strArr = str.split('');
    const strArrRev = [...strArr].reverse();

    for (let i = 0; i < strArr.length; i++) {
        if (strArr[i] !== strArrRev[i]) {
            return false;
        }
    }
    return true;
}

console.log(isPalindrome('saippuakauppias'));
console.log(isPalindrome("saippuakäpykauppias"));