let x = 0;
let y = 0;
const commandList = 'NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE';

const directionsMapping = new Map();
directionsMapping.set('N', 0);
directionsMapping.set('E', 1);
directionsMapping.set('S', 2);
directionsMapping.set('W', 3);
directionsMapping.set('C', 4);
directionsMapping.set('B', 5);

let commadsAsNumbers = [];

for (const cmd of commandList) {
    commadsAsNumbers.push(directionsMapping.get(cmd));
}

const incrementFunctions = [
    () => y++,
    () => x++,
    () => y--,
    () => x--,
    () => {},
];

for (let i = 0; i < commadsAsNumbers.length; i++) {
    const cmd = commadsAsNumbers[i];
    if (cmd === 5) {
        break;
    }
    incrementFunctions[cmd]();
}

console.log('x', x);
console.log('y', y);
