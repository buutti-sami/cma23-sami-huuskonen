const getCountOfLetters = (str) => {
    const alphabet = [];
    // letters from a to z
    for (let i = 97; i <= 122; i++) {
        alphabet.push(String.fromCharCode(i));
    }

    const charIndex = {};

    for (const letter in alphabet) {
        charIndex[alphabet[letter]] = parseInt(letter);
    }

    let = letterCounts = [];

    for (const letter of alphabet) {
        const lowerCaseStr = str.toLowerCase();
        const letterRegex = new RegExp(letter, 'g');
        const lettersFound = lowerCaseStr.match(letterRegex);
        if (lettersFound) {
            letterCounts.push(lettersFound.length);
        } else {
            letterCounts.push(0);
        }
    }
    return letterCounts;
};

const result = getCountOfLetters('a black cat');
console.log(result); // prints [ 3, 1, 2, 0, 0, 0, 0, 0, 0, 0, 1, 1, ... 1, 0, 0, 0, 0, 0, 0  ]
// corresponding letters:    a  b  c  d  e  f  g, h, i, j, k, l, ... t, u, v, w, x, y, z
