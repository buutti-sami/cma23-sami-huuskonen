function numberRange(start, end) {
    const numbers = [];
    
    if (start < end) {
        for (let i = start; i <= end; i++) {
            numbers.push(i);         
        }
    } else if (start > end) {
        for (let i = start; i >= end ; i--) {
            numbers.push(i)
            
        }
    }
    return numbers;
}

console.log(numberRange(1, 5));   // prints [ 1, 2, 3, 4, 5 ]
console.log(numberRange(-5, -1)); // prints [ -5, -4, -3, -2, -1 ]
console.log(numberRange(9, 5));   // prints [ 9, 8, 7, 6, 5 ]
