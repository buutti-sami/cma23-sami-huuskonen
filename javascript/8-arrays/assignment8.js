const reverseWords = (str) =>
    str
        .split(' ')
        .map((w) => [...w].reverse().join(''))
        .join(' ');

const sentence = 'this is a short sentence';
const reversed = reverseWords(sentence);
console.log(reversed); // prints "siht si a trohs ecnetnes"
