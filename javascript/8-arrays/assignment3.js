function insertNumber(arr, num) {
    arr.push(num);
    arr.sort((a, b) => a - b);
    return arr;
}

const array = [ 1, 3, 4, 7, 11 ];
insertNumber(array, 8);
console.log(array); // prints [ 1, 3, 4, 7, 8, 11 ] 
insertNumber(array, 90);
console.log(array); // prints [ 1, 3, 4, 7, 8, 11, 90 ]
