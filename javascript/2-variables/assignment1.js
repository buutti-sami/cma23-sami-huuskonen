let myName = "Sami Huuskonen";
let age = 33;
let favProgLang = "TypeScript";

console.log(`I'm ${myName} and I'm ${age} years old and my favorite programming language is ${favProgLang}`);

myName = "Bill Gates";
age = 51;
favProgLang = "C++";

console.log(`I'm ${myName} and I'm ${age} years old and my favorite programming language is ${favProgLang}`);
