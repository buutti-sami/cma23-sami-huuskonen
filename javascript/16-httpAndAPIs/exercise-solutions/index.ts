import axios, { AxiosResponse } from 'axios';

const baseURL = 'https://jsonplaceholder.typicode.com';

// Exercise 1
function execExerciseOne() {
    const getUserById = async (id: number) => {
        try {
            const response = await fetch(`${baseURL}/users/${id}`);
            const user = await response.json();
            return user;
        } catch (error) {
            console.log(error);
        }
    };

    const getPostById = async (id: number) => {
        try {
            const response = await fetch(`${baseURL}/posts/${id}`);
            const post = await response.json();

            const user = await getUserById(post.userId);
            console.log(`Post #${id} by ${user.name}: ${post.title}`);
        } catch (error) {
            console.log(error);
        }
    };

    getPostById(5);
}

// execExerciseOne();

// Exercise 2: Reading comments
function execExerciseTwo() {
    const getPostComments = async (id: number): Promise<void> => {
        try {
            const response = await axios.get(`${baseURL}/comments`, {
                params: { postId: id },
            });
            console.log(response.data);
        } catch (error) {
            console.log(error);
        }
    };

    getPostComments(5);
}

// execExerciseTwo();

// Exercise 3: Creating & Updating
async function execExerciseThree() {
    interface Post {
        userId: number;
        title: string;
        body: string;
        id?: number;
    }

    const addNewPost = async (post: Post): Promise<Post | undefined> => {
        try {
            const response: AxiosResponse<Post> = await axios.post<Post>(
                `${baseURL}/posts`,
                {
                    ...post,
                }
            );

            console.log(response.status);
            const addedPost: Post = response.data;
            return addedPost;
        } catch (error) {
            console.log(error);
        }
    };

    const updatePost = async (
        id: number,
        post: Post
    ): Promise<Post | undefined> => {
        try {
            const response = await fetch(`${baseURL}/posts/${id}`, {
                method: 'PUT',
                body: JSON.stringify(post),
            });
            console.log(response.status);
            const updatedPost = await response.json();
            return updatedPost;
        } catch (error) {
            console.log('error: ', error);
        }
    };

    const addedPost = await addNewPost({
        userId: 5,
        title: 'Moi',
        body: 'moimoimoimoimoimoi',
    });

    console.log(addedPost);

    if (addedPost) {
        const updatedPost = await updatePost(addedPost.id!, {
            ...addedPost,
            title: 'Greeting',
        });
        console.log(updatedPost);
    }
}

// execExerciseThree();
