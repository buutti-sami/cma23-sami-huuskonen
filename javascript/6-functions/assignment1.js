const language = 'it';

/**
 *
 * @param {string} lang
 */
function hello(lang) {
  switch (lang) {
    case 'fi':
      console.log('Hei muailma!');
      break;
    case 'fr':
      console.log('Bonjour le Monde');
      break;
    case 'it':
      console.log('Ciao Mondo!');
      break;
    default:
      console.log('language not supported');
      break;
  }
}
hello(language);
