const firstTriangle = { width: 7.0, length: 3.5 };
const secondTriangle = { width: 4.3, length: 6.4 };
const thirdTriangle = { width: 5.5, length: 5.0 };

const calculateTriangleArea = (triangle) => {
    return triangle.width * triangle.length / 2
}

const firstTriangleArea = calculateTriangleArea(firstTriangle);

const secondTriangleArea = calculateTriangleArea(secondTriangle);

const thirdTriangleArea = calculateTriangleArea(thirdTriangle);

console.log("Area of first triangle: " + firstTriangleArea);
console.log("Area of second triangle: " + secondTriangleArea);
console.log("Area of third triangle: " + thirdTriangleArea);
