const playerOneIncome = 15400;
const playerTwoIncome = 3500;

const differenceInIncome = playerOneIncome - playerTwoIncome;

const tax = Math.random().toFixed(1)
const playerOneIncomeAfterTax = playerOneIncome ** tax;
const playerTwoIncomeAfterTax = playerTwoIncome ** tax;

console.log('Player one income after tax: ', playerOneIncomeAfterTax);
console.log('Player two income after tax: ', playerTwoIncomeAfterTax);
