const distance = 120;
const speed = 50;

const timeToTravel = distance / speed;
console.log(`To travel ${distance}km at the speed of ${speed}km/h takes ${timeToTravel}h`);
