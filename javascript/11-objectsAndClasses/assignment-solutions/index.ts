// Assignment 1
interface GradesToScores {
    S: number;
    A: number;
    B: number;
    C: number;
    D: number;
    F: number;
    [grade: string]: number;
}

// a)
function calculateTotalScore(grades: string) {
    const gradesToScores: GradesToScores = {
        S: 8,
        A: 6,
        B: 4,
        C: 3,
        D: 2,
        F: 0,
    };
    return grades.split('').reduce((acc: number, curr: string) => {
        acc += gradesToScores[curr];
        return acc;
    }, 0);
}

const totalScore = calculateTotalScore('DFCBDABSB');
console.log(totalScore); // prints 33

// b)
const calculateAverageScore = (grades: string) =>
    calculateTotalScore(grades) / grades.length;

const averageScore = calculateAverageScore('DFCBDABSB');
console.log(averageScore); // prints 3.6666666666666665

// c)
const sequenceOfGrades = ['AABAACAA', 'FFDFDCCDCB', 'ACBSABA', 'CCDFABABC'];
const sequanceOfAverageScores = sequenceOfGrades.map((grade) =>
    calculateAverageScore(grade)
);
console.log(sequanceOfAverageScores);

// Assignment 2 (Dictionary)
// a)
interface Translations {
    [key: string]: string;
}

const translations: Translations = {
    hello: 'hei',
    world: 'maailma',
    bit: 'bitti',
    byte: 'tavu',
    integer: 'kokonaisluku',
    boolean: 'totuusarvo',
    string: 'merkkijono',
    network: 'verkko',
};

// b)
function printTranslatableWords(translations: Translations) {
    console.log(Object.keys(translations));
}

printTranslatableWords(translations);

// c)
function translate(word: string): string {
    return word in translations
        ? translations[word]
        : 'No translation exists for word word given as the argument';
}

console.log(translate('network'));

// d)
console.log(translate('consciousness'));

// Assignment 3
function getCountLetters(str: string) {
    const formattedStr = str.replace(/\s+/g, '').toLowerCase();
    interface LetterCounts {
        [key: string]: number;
    }
    let letterCounts: LetterCounts = {};

    for (const char of formattedStr) {
        if (char in letterCounts) {
            letterCounts[char] += 1;
        } else {
            letterCounts[char] = 1;
        }
    }

    const charactersSorted = Object.keys(letterCounts).sort();
    interface ReturnValue {
        [key: string]: number;
    }
    const returnValue: ReturnValue = {};
    charactersSorted.forEach(
        (char) => (returnValue[char] = letterCounts[char])
    );

    return returnValue;
}

console.log(getCountLetters('a black cat'));

// Assignment 4 (Command list, now with an object)
let x = 0;
let y = 0;
const commandList = 'NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE';

interface CommandHandlers {
    N: () => void;
    E: () => void;
    S: () => void;
    W: () => void;
    [key: string]: () => void;
}

const commandHandlers: CommandHandlers = {
    N: () => y++,
    E: () => x++,
    S: () => y--,
    W: () => x--,
};

for (const cmd of commandList) {
    if (cmd === 'C') {
        continue;
    } else if (cmd === 'B') {
        break;
    }
    commandHandlers[cmd]();
}

console.log('x', x);
console.log('y', y);

// Assignment 5 (Room class)
// a)

class Room {
    width: number;
    height: number;
    furniture: string[];

    constructor(width: number, height: number) {
        this.width = width;
        this.height = height;
        this.furniture = [];
    }

    getArea(): number {
        return this.width * this.height;
    }

    addFurniture(furniture: string): void {
        this.furniture.push(furniture);
    }
}

const room = new Room(4.5, 6.0);
console.log(room); // Room { width: 4.5, height: 6 }

// b)
const area = room.getArea();
console.log(area); // prints 27

// c)
room.addFurniture('sofa');
room.addFurniture('bed');
room.addFurniture('chair');
console.log(room); // prints Room { width: 4.5, height: 6, furniture: [ 'sofa', 'bed', 'chair' ] }

// Assignment 6 (Command list, with a class)
class SuperRobot {
    x: number;
    y: number;

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }

    handleCommandList(commandList: string) {
        const commandHandlers: CommandHandlers = {
            N: () => this.y++,
            E: () => this.x++,
            S: () => this.y--,
            W: () => this.x--,
        };

        for (const cmd of commandList) {
            if (cmd === 'C') {
                continue;
            } else if (cmd === 'B') {
                break;
            }
            commandHandlers[cmd]();
        }
    }
}

const superRobot = new SuperRobot(0, 0);
superRobot.handleCommandList(commandList);
console.log('SuperRobot x:', superRobot.x);
console.log('SuperRobot y:', superRobot.y);

// Assignment 7 (Animals)
// a)
class Animal {
    weight: number;
    cuteness: number;

    constructor(weight: number, cuteness: number) {
        this.weight = weight;
        this.cuteness = cuteness;
    }

    makeSound() {
        console.log('silence');
    }
}

const babyTiger = new Animal(18, 99);
console.log('Baby tiger: ', babyTiger);

// b)
class Cat extends Animal {
    constructor(weight: number, cuteness: number) {
        super(weight, cuteness);
    }

    makeSound(): void {
        console.log('meow');
    }
}

const bigCat = new Cat(15, 40);
bigCat.makeSound();
console.log('bigCat', bigCat);

// c)
class Dog extends Animal {
    breed: string;

    constructor(weight: number, cuteness: number, breed: string) {
        super(weight, cuteness);
        this.breed = breed;
    }

    makeSound(): void {
        if (this.cuteness > 4) {
            console.log('awoo');
        } else {
            console.log('bark');
        }
    }
}

const dog1 = new Dog(7.0, 4.5, 'kleinspitz');
const dog2 = new Dog(30.0, 3.75, 'labrador');
dog1.makeSound(); // prints "awoo"
dog2.makeSound(); // prints "bark"

// Assignment 8 (Weather events)
// a)
class WeatherEvent {
    timestamp: string;

    constructor(timestamp: string) {
        this.timestamp = timestamp;
    }

    getInformation(): string {
        return '';
    }

    print(): void {
        console.log(`${this.timestamp} ${this.getInformation()}`);
    }
}

// b)
class TemperatureChangeEvent extends WeatherEvent {
    temperature: number;

    constructor(timestamp: string, temperature: number) {
        super(timestamp);
        this.temperature = temperature;
    }

    getInformation(): string {
        return `temperature: ${this.temperature}°C`;
    }
}

// c)
class HumidityChangeEvent extends WeatherEvent {
    humidity: number;

    constructor(timestamp: string, humidity: number) {
        super(timestamp);
        this.humidity = humidity;
    }

    getInformation(): string {
        return `humidity: ${this.humidity}%`;
    }
}

// d)
class WindStrengthChangeEvent extends WeatherEvent {
    wind: number;

    constructor(timestamp: string, wind: number) {
        super(timestamp);
        this.wind = wind;
    }

    getInformation(): string {
        return `wind strength: ${this.wind} m/s`;
    }
}

// e)
const weatherEvents = [];

weatherEvents.push(new TemperatureChangeEvent('2022-11-29 03:00', -6.4));
weatherEvents.push(new HumidityChangeEvent('2022-11-29 04:00', 95));
weatherEvents.push(new WindStrengthChangeEvent('2022-11-30 13:00', 2.2));

weatherEvents.forEach((weatherEvent) => weatherEvent.print());
// Should print:
// 2022-11-29 03:00 temperature: -6.4°C
// 2022-11-29 04:00 humidity: 95%
// 2022-11-30 13:00 wind strength: 2.2 m/s
