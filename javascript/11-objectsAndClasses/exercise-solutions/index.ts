// Exercise 1
interface FruitsWithWeights {
    [key: string]: number;
}
const fruitsWithWeights: FruitsWithWeights = {
    banana: 118,
    apple: 85,
    mango: 200,
    lemon: 65,
};

function printWeight(fruit: string) {
    if (fruit in fruitsWithWeights) {
        const fruitWeight = fruitsWithWeights[fruit];
        console.log(`${fruit} weighs ${fruitWeight} grams`);
    } else {
        console.log(
            `Fruit not found. Supported fruits are: ${Object.keys(
                fruitsWithWeights
            )}`
        );
    }
}

printWeight('banana');

// Exercise 2: Objects in objects
interface CourseGrade {
    [key: string]: number;
}

interface Student {
    name: string;
    credits: number;
    courseGrades: CourseGrade;
}

const student: Student = {
    name: 'Sam',
    credits: 79,
    courseGrades: {
        'Introduction to Neuroscience': 5,
        'Advanced Morality': 5,
        'Elements of AI': 4,
        'JavaScript Basics': 1,
    },
};

console.log(student);

const updatedStudent: Student = {
    ...student,
    courseGrades: { ...student.courseGrades },
};
updatedStudent.courseGrades['Program Design'] = 3;
console.log(student);
console.log(updatedStudent);

updatedStudent.courseGrades['JavaScript Basics'] = 4;
console.log(updatedStudent);

// Exercise 3: Arrays in objects
interface Course {
    name: string;
    grade: number;
}
interface StudentExThree {
    name: string;
    credits: number;
    courses: Course[];
}
const studentExThree: StudentExThree = {
    name: 'Aili',
    credits: 100,
    courses: [
        { name: 'Intro to Programming', grade: 3 },
        { name: 'Advanced Java', grade: 5 },
    ],
};
const course = studentExThree.courses.find(
    (course) => course.name === 'Intro to Programming'
);
console.log(`${studentExThree.name} got ${course?.grade} from ${course?.name}`);

function addCourse(courseName: string, courseGrade: number) {
    studentExThree.courses = [
        ...studentExThree.courses,
        { name: courseName, grade: courseGrade },
    ];
}
addCourse('The Basics of Machine Learning', 2);
console.log(studentExThree);

// Exercise 4: Functions in objects
interface Processor {
    name: string;
    cache: string;
    clockSpeed: number;
    overclock: () => void;
    savePower: () => void;
}

const processor: Processor = {
    name: 'Buutti SuperCalculator 6000',
    cache: '96 GB',
    clockSpeed: 9001.0,
    overclock: function () {
        this.clockSpeed += 500;
    },
    savePower: function () {
        if (this.clockSpeed > 2000) {
            this.clockSpeed = 2000;
        } else {
            this.clockSpeed /= 2;
        }
    },
};

console.log('Processor before overclocking: ', processor);
processor.overclock();
console.log('Processor after overclocking and before savePower: ', processor);
processor.savePower();
console.log('Processor after savePower', processor);
processor.savePower();
console.log('Processor after savePower on clockspeed 2000', processor);

// Exercise 5: Class and constructor
class Shape {
    width: number;
    height: number;

    constructor(width: number, height: number) {
        this.width = width;
        this.height = height;
    }

    getArea(): number {
        return 0;
    }
}
class Rectangle extends Shape {
    constructor(width: number, height: number) {
        super(width, height);
    }

    getArea(): number {
        return this.height * this.width;
    }
}

const rectOne = new Rectangle(40000, 2);
const rectTwo = new Rectangle(2, 2);
const rectThree = new Rectangle(100, 100);

console.log('rectOne', rectOne);
console.log('rectTwo', rectTwo);
console.log('rectThree', rectThree);

// Exercise 6: Functions in classes
console.log('rectThree area: ', rectThree.getArea());

// Exercise 7: Inheritance
class Ellipse extends Shape {
    constructor(width: number, height: number) {
        super(width, height);
    }

    getArea(): number {
        return (((Math.PI * this.width) / 2) * this.height) / 2;
    }
}

class Triangle extends Shape {
    constructor(width: number, height: number) {
        super(width, height);
    }

    getArea(): number {
        return (this.width * this.height) / 2;
    }
}

const shapeOne = new Shape(10, 20);
const shapeOneArea = shapeOne.getArea();
const ellipseBig = new Ellipse(5000, 665);
const ellipseBigArea = ellipseBig.getArea();
const triangle = new Triangle(10, 50);
const triangleArea = triangle.getArea();

console.log('shapeOneArea', shapeOneArea);
console.log('ellipseBigArea', ellipseBigArea);
console.log('triangleArea', triangleArea);

// Exercise 8: Nested inheritance
class Square extends Rectangle {
    sideLength: number;

    constructor(sideLength: number) {
        super(sideLength, sideLength);

        this.sideLength = sideLength;
    }

    getArea(): number {
        return this.sideLength ** 2;
    }
}

class Circle extends Ellipse {
    diameter: number;

    constructor(diameter: number) {
        super(diameter, diameter);

        this.diameter = diameter;
    }

    getArea(): number {
        return Math.PI * (this.diameter / 2) ** 2;
    }
}

const smallSquare = new Square(3);
const smallSquareArea = smallSquare.getArea();
console.log('smallSquareArea', smallSquareArea);

const circle = new Circle(32);
const circleArea = circle.getArea();
console.log('circleArea', circleArea);

// Exercise 9: super
type Message =
    | 'moveSouth'
    | 'moveNorth'
    | 'moveEast'
    | 'moveWest'
    | 'moveNE'
    | 'moveSE'
    | 'moveNW'
    | 'moveSW';
class Robot {
    x: number;
    y: number;

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }

    handleMessage(message: Message): void {
        switch (message) {
            case 'moveSouth':
                this.y -= 1;
                break;
            case 'moveNorth':
                this.y += 1;
                break;
            case 'moveEast':
                this.x += 1;
                break;
            case 'moveWest':
                this.y -= 1;
                break;
            default:
                break;
        }
    }
}

class FlexibleRobot extends Robot {
    constructor(x: number, y: number) {
        super(x, y);
    }

    handleMessage(message: Message): void {
        switch (message) {
            case 'moveNE':
                this.x += 1;
                this.y += 1;
                break;
            case 'moveNW':
                this.x -= 1;
                this.y += 1;
                break;
            case 'moveSE':
                this.x += 1;
                this.y -= 1;
                break;
            case 'moveSW':
                this.x -= 1;
                this.y -= 1;
                break;

            default:
                super.handleMessage(message);
                break;
        }
    }
}

const flexibleRobot = new FlexibleRobot(0, 0);
console.log(flexibleRobot);
flexibleRobot.handleMessage('moveNE');
flexibleRobot.handleMessage('moveNE');
flexibleRobot.handleMessage('moveEast');
flexibleRobot.handleMessage('moveEast');
console.log(flexibleRobot);
