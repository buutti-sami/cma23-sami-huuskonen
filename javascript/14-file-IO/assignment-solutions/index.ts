import readline from 'readline-sync';
import fs from 'fs';

function readTextFile(path: string) {
    try {
        const file = fs.readFileSync(path, 'utf-8');
        console.log(file);
    } catch (error) {
        console.log(error);
    }
}

console.log(
    'Hi! I am a dumb chat bot You can check all the things I can do by typing "help".'
);

interface WeatherChoices {
    temperatures: number[];
    cloudy: string[];
    sunny: string[];
    wind: string[];
}

const yesOrNo = ['Yes', 'No'];

const weatherChoices: WeatherChoices = {
    temperatures: Array.from({ length: 31 }, (_, index) => index),
    cloudy: yesOrNo,
    sunny: yesOrNo,
    wind: yesOrNo,
};

let cmdCounter = 0;
let botName = 'AI-Master-Overlord';

function sayHello() {
    const userInput = readline.question('What is your name? ');
    console.log(`Hello there, ${userInput}`);
}

function renameBot() {
    const botNameUserInput = readline
        .question('Type my new name, please. ')
        .toLowerCase();
    const isUserHappyInput = readline.question(
        `Are you happy with the name ${botNameUserInput}? (Yes / No) `
    );

    if (isUserHappyInput === 'yes') {
        botName = botNameUserInput;
        console.log(`I was renamed ${botNameUserInput}`);
    } else if (isUserHappyInput === 'no') {
        console.log(`Name not changed. My name is ${botName}.`);
    }
}

function generateForecast() {
    console.log('Tomorrows weather will be...');
    const randomTemperature =
        weatherChoices.temperatures[
            Math.floor(Math.random() * weatherChoices.temperatures.length - 1)
        ];
    const zeroOrOne = () => Math.floor(Math.random() * 2);
    console.log(`Temperature: ${randomTemperature}`);
    console.log(`Cloudy: ${weatherChoices.cloudy[zeroOrOne()]}`);
    console.log(`Sunny: ${weatherChoices.sunny[zeroOrOne()]}`);
    console.log(`Wind: ${weatherChoices.wind[zeroOrOne()]}`);
}

function askQuestion() {
    const userInput = readline.question();

    switch (userInput) {
        case 'quit':
            console.log('bye');
            break;
        case 'help':
            console.log('case help');
            readTextFile('./data/helpText.txt');
            cmdCounter++;
            break;
        case 'hello':
            sayHello();
            cmdCounter++;
            break;
        case 'botInfo':
            console.log(
                `I am a dumb bot. You can ask me almost anything :). You have already asked me ${cmdCounter} questions.`
            );
            cmdCounter++;
            break;
        case 'botName':
            console.log(
                `My name is currently ${botName}. If you want to change it, type botRename.`
            );
            cmdCounter++;
            break;
        case 'botRename':
            renameBot();
            cmdCounter++;
            break;
        case 'forecast':
            generateForecast();
            cmdCounter++;
            break;
        default:
            break;
    }
    if (userInput !== 'quit') {
        askQuestion();
    }
}

askQuestion();
