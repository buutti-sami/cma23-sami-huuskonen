import fs from 'fs';

// Exercise 1
fs.readFile('./data/textFile.txt', 'utf-8', (err, data) => {
    if (err) {
        console.log(err);
    }

    const words = data.split(' ');

    const alteredText = words
        .map((word) => {
            if (word === 'joulu') {
                return 'kinkku';
            } else if (word === 'lapsilla') {
                return 'poroilla';
            } else {
                return word;
            }
        })
        .join(' ');

    const writeFilePath = './data/alteredTextFile.txt';
    fs.writeFile(writeFilePath, alteredText, 'utf-8', (err) => {
        if (err) {
            console.log(err);
        } else {
            console.log(`File written successfully in ${writeFilePath}`);
        }
    });
});

// Exercise 2
interface Forecast {
    day: string;
    temperature: number;
    cloudy: boolean;
    sunny: boolean;
    windy: boolean;
}
const forecast: Forecast = {
    day: 'monday',
    temperature: 20,
    cloudy: true,
    sunny: false,
    windy: false,
};

fs.writeFileSync(
    './data/forecast_data.json',
    JSON.stringify(forecast),
    'utf-8'
);
