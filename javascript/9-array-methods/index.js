// Exercise 9
const ages = [13, NaN, 21, 33, 17, 46];

const sum = ages.reduce((acc, curr) => (isNaN(curr) ? acc : curr + acc), 0);

console.log(sum);

// *********************************************************************

// Assignment 1 (Find elements)
const numbers = [8, 12, 17, 9, 16, 24, 16, 25, 35, 27, 38, 50];

// a)
let firstNumAbove20WithLoop = null;

for (n of numbers) {
    if (n > 20) {
        firstNumAbove20WithLoop = n;
        break;
    }
}
console.log('firstNumAbove20WithLoop', firstNumAbove20WithLoop);

// b)
const firstNumAbove20WithFind = numbers.find((n) => n > 20);
console.log('firstNumAbove20WithFind', firstNumAbove20WithFind);

// c)
const indexOfFirstNumAbove20 = numbers.findIndex((n) => n > 20);
console.log(indexOfFirstNumAbove20);

// d)
numbers.splice(indexOfFirstNumAbove20 + 1);
console.log('numbers', numbers);

// Assignment 2 (Game list)
const games = [
    { id: 1586948654, date: '2022-10-27', score: 145, won: false },
    { id: 2356325431, date: '2022-10-30', score: 95, won: false },
    { id: 2968411644, date: '2022-10-31', score: 180, won: true },
    { id: 1131684981, date: '2022-11-01', score: 210, won: true },
    { id: 1958468135, date: '2022-11-01', score: 111, won: true },
    { id: 2221358512, date: '2022-11-02', score: 197, won: false },
    { id: 1847684969, date: '2022-11-03', score: 203, won: true },
];

// a)
const gameId = 1958468135;
const game = games.find((g) => (g.id = gameId));
console.log(game);

// b)
const indexOfFirstGamePlayerWon = games.findIndex((g) => g.won === true);
console.log(indexOfFirstGamePlayerWon);

// Assignment 3 (Average scores)

// a)
const wonGames = games.filter((g) => g.won === true);
const calculateAverageScore = (games) => {
    const gamesTotalScore = games.reduce((acc, curr) => {
        acc += curr.score;
        return acc;
    }, 0);
    return gamesTotalScore / games.length;
};
const playersAvgWonScore = calculateAverageScore(wonGames);
console.log(playersAvgWonScore);

// b)
const lostGames = games.filter((g) => g.won === false);
const playersAvgLostScore = calculateAverageScore(lostGames);
console.log(playersAvgLostScore);

// Assignment 4 (Increment / Decrement)

// a)
const assignmentFourNumbers = [4, 7, 1, 8, 5];
const incrementAll = (arr) => arr.map((n) => n + 1);
const assignmentFourNumbersIncremented = incrementAll(assignmentFourNumbers);
console.log(assignmentFourNumbersIncremented); // prints [ 5, 8, 2, 9, 6 ]

// b)
const decrementAll = (arr) => arr.map((n) => n - 1);
const assignmentFourNumbersDecremented = decrementAll(assignmentFourNumbers);
console.log(assignmentFourNumbersDecremented); // prints [ 3, 6, 0, 7, 4 ]

// Assignment 5 (Grades)

const students = [
    { name: 'Sami', score: 24.75 },
    { name: 'Heidi', score: 20.25 },
    { name: 'Jyrki', score: 27.5 },
    { name: 'Helinä', score: 26.0 },
    { name: 'Maria', score: 17.0 },
    { name: 'Yrjö', score: 14.5 },
];

function getGrades(students) {
    const scoreToGrade = (score) => {
        if (score < 14.0) {
            return 0;
        } else if (score <= 17.0) {
            return 1;
        } else if (score <= 20.0) {
            return 2;
        } else if (score <= 23.0) {
            return 3;
        } else if (score <= 26.0) {
            return 4;
        } else if (score > 26.0) {
            return 5;
        }
    };

    const studentScores = students.map((student) => {
        return { name: student.name, grade: scoreToGrade(student.score) };
    });
    return studentScores;
}
console.log(getGrades(students));

// Assignment 6 (Cleanup)

const objectArray = [
    { x: 14, y: 21, type: 'tree', toDelete: false },
    { x: 1, y: 30, type: 'house', toDelete: false },
    { x: 22, y: 10, type: 'tree', toDelete: true },
    { x: 5, y: 34, type: 'rock', toDelete: true },
    null,
    { x: 19, y: 40, type: 'tree', toDelete: false },
    { x: 35, y: 35, type: 'house', toDelete: false },
    { x: 19, y: 40, type: 'tree', toDelete: true },
    { x: 24, y: 31, type: 'rock', toDelete: false },
];

// a)
let newObjectArrayWithLoop = [];
for (const obj of objectArray) {
    if (obj) {
        if (obj.toDelete === true) {
            newObjectArrayWithLoop.push(null);
        } else {
            newObjectArrayWithLoop.push(obj);
        }
    } else {
        newObjectArrayWithLoop.push(obj);
    }
}
console.log(newObjectArrayWithLoop);

// b)
const newObjectArrayWithMap = objectArray.map((obj) => {
    if (obj) {
        if (obj.toDelete === true) {
            return null;
        } else {
            return obj;
        }
    }
    return obj;
});
console.log(newObjectArrayWithMap);

// c)
/* 
Creating a new array with for example Array.map() doesn't usually create performance issues but with 100.000 entries it could.
Especially if this new array needs to be constructed very often. I think that with Array.map()
a new place in memory needs to be allocated every time the array is created.
When using a loop that mutates an object in memory (array), new memory allocation isn't necessary and therefore
it is more effecient. 
*/

// Assignment 7 (Reduce)

// 1)
function total(arr) {
    return arr.reduce((acc, curr) => {
        acc += curr;
        return acc;
    }, 0);
}

console.log(total([1, 2, 3])); // 6

// 2)
function stringConcat(arr) {
    return arr.reduce((acc, curr) => {
        console.log('acc', acc);
        console.log('curr', curr);
        acc += curr.toString();
        return acc;
    }, '');
}

console.log(stringConcat([1, 2, 3])); // "123"

// 3
function totalVotes(arr) {
    return arr.reduce((acc, curr) => {
        if (curr.voted) {
            acc++;
        }
        return acc;
    }, 0);
}

var voters = [
    { name: 'Bob', age: 30, voted: true },
    { name: 'Jake', age: 32, voted: true },
    { name: 'Kate', age: 25, voted: false },
    { name: 'Sam', age: 20, voted: false },
    { name: 'Phil', age: 21, voted: true },
    { name: 'Ed', age: 55, voted: true },
    { name: 'Tami', age: 54, voted: true },
    { name: 'Mary', age: 31, voted: false },
    { name: 'Becky', age: 43, voted: false },
    { name: 'Joey', age: 41, voted: true },
    { name: 'Jeff', age: 30, voted: true },
    { name: 'Zack', age: 19, voted: false },
];
console.log(totalVotes(voters)); // 7

// 4
function shoppingSpree(arr) {
    return arr.reduce((acc, curr) => {
        acc += curr.price;
        return acc;
    }, 0);
}

var wishlist = [
    { title: 'Tesla Model S', price: 90000 },
    { title: '4 carat diamond ring', price: 45000 },
    { title: 'Fancy hacky Sack', price: 5 },
    { title: 'Gold fidgit spinner', price: 2000 },
    { title: 'A second Tesla Model S', price: 90000 },
];

console.log(shoppingSpree(wishlist)); // 227005

// 5
function flatten(arr) {
    return arr.reduce((acc, curr) => {
        acc = acc.concat(...curr);
        return acc;
    }, []);
}

var arrays = [['1', '2', '3'], [true], [4, 5, 6]];

console.log(flatten(arrays)); // ["1", "2", "3", true, 4, 5, 6];

// 6
var voters = [
    { name: 'Bob', age: 30, voted: true },
    { name: 'Jake', age: 32, voted: true },
    { name: 'Kate', age: 25, voted: false },
    { name: 'Sam', age: 20, voted: false },
    { name: 'Phil', age: 21, voted: true },
    { name: 'Ed', age: 55, voted: true },
    { name: 'Tami', age: 54, voted: true },
    { name: 'Mary', age: 31, voted: false },
    { name: 'Becky', age: 43, voted: false },
    { name: 'Joey', age: 41, voted: true },
    { name: 'Jeff', age: 30, voted: true },
    { name: 'Zack', age: 19, voted: false },
];

function voterResults(arr) {
    const countVotes = (voters) => {
        return voters.reduce((acc, curr) => {
            if (curr.voted) {
                acc++;
            }
            return acc;
        }, 0);
    };

    const isYoungAge = (voter) => voter.age >= 18 && voter.age <= 25;
    const isMidAge = (voter) => voter.age > 25 && voter.age <= 35;
    const isOldAge = (voter) => voter.age > 35 && voter.age <= 55;

    const youngPeople = arr.filter((v) => isYoungAge(v));
    const numYoungPeople = youngPeople.length;
    const numYoungVotes = countVotes(youngPeople);

    const midPeople = arr.filter((v) => isMidAge(v));
    const numMidsPeople = midPeople.length;
    const numMidVotesPeople = countVotes(midPeople);

    const oldPeople = arr.filter((v) => isOldAge(v));
    const numOldsPeople = youngPeople.length;
    const numOldVotesPeople = countVotes(oldPeople);

    let votes = {
        numYoungVotes,
        numYoungPeople,
        numMidVotesPeople,
        numMidsPeople,
        numOldVotesPeople,
        numOldsPeople,
    };

    return votes;
}

console.log(voterResults(voters)); // Returned value shown below:
/*
{ numYoungVotes: 1,
  numYoungPeople: 4,
  numMidVotesPeople: 3,
  numMidsPeople: 4,
  numOldVotesPeople: 3,
  numOldsPeople: 4 
}
*/
