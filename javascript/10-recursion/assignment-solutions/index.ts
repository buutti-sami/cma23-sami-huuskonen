// Assignment 1
function calcNum(n: number): number {
    if (n <= 0) {
        return 0;
    } else if (n === 1) {
        return 1;
    }

    return (calcNum(n - 2) * 3) + calcNum(n - 1);
}

console.log(calcNum(17));


// Assignment 2 (Intermediate recursion)
function sentencify(words: string[], index: number): string {
    if (index >= words.length) {
        return '!';
    }
    
    const restOfSentence = sentencify(words, index + 1);
    
    return ' ' + words[index] +  restOfSentence;
}

const wordArray = [ "The", "quick", "silver", "wolf" ];
console.log(sentencify(wordArray, 0)); // prints "The quick silver wolf!"
console.log(sentencify(wordArray, 1)); // prints "quick silver wolf!"


// EXTRA: Assignment 3 (Harder recursion with merge-sort)
function mergeSort(array: number[]): number[] {
    if (array.length <= 1) {
        return array;
    }

    const right = array.slice((array.length % 2) === 0 ? array.length / 2 : Math.floor(array.length / 2));
    const left = array.slice(0, array.length / 2);

    const sortedRight = mergeSort(right);
    const sortedLeft = mergeSort(left);

    return mergeSubLists(sortedLeft, sortedRight);
}



function mergeSubLists(leftList: number[], rightList: number[]): number[] {
    let mergedList: number[] = []
    let leftIndex = 0;
    let rightIndex = 0;

    while (leftIndex < leftList.length && rightIndex < rightList.length) {
        if (leftList[leftIndex] < rightList[rightIndex]) {
            mergedList.push(leftList[leftIndex]);
            leftIndex++;
        } else {
            mergedList.push(rightList[rightIndex]);
            rightIndex++;
        }
    }
    return mergedList.concat(leftList.slice(leftIndex)).concat(rightList.slice(rightIndex));
}


const array = [ 4, 19, 7, 1, 9, 22, 6, 13 ];
const sorted = mergeSort(array);
console.log("sorted: ", sorted); // prints [ 1, 4, 6, 7, 9, 13, 19, 22 ]

const unevenArray = [ 4, 19, 7, 1, 11, 9, 22, 6, 13 ];
const sortedUneven = mergeSort(unevenArray);
console.log("sortedUneven: ", sortedUneven); // prints [ 1, 4, 6, 7, 9, 13, 19, 22 ]



// Assignment 4 (Recursive structure)
interface UIElement {
    name: string;
    width: number;
    height: number;
    children: UIElement[]
}

function buildUserInterface() {
    const mainWindow: UIElement = { name: "MainWindow", width: 600, height: 400, children: [ ] };
    const buttonExit: UIElement = { name: "ButtonExit", width: 100, height: 30, children: [ ] };
    mainWindow.children.push(buttonExit);

    const settingsWindow: UIElement = { name: "SettingsWindow", width: 400, height: 300, children: [ ] };
    const buttonReturnToMenu: UIElement = { name: "ButtonReturnToMenu", width: 100, height: 30, children: [ ] };
    settingsWindow.children.push(buttonReturnToMenu);
    mainWindow.children.push(settingsWindow);

    const profileWindow: UIElement = { name: "ProfileWindow", width: 500, height: 400, children: [ ] };
    const profileInfoPanel: UIElement = { name: "ProfileInfoPanel", width: 200, height: 200, children: [ ] };
    profileWindow.children.push(profileInfoPanel);
    mainWindow.children.push(profileWindow);

    return mainWindow;
}

const userInterfaceTree = buildUserInterface();
console.log(userInterfaceTree);

function findControl(control: UIElement, name: string): UIElement | null {
    if (control.name === name) {
        return control;
    } else {
        for (const child of control.children) {
            const result = findControl(child, name);
            if (result !== null) {
                return result;
            }
        }
        return null;
    }
}

const profileInfoPanel = findControl(userInterfaceTree, "ProfileInfoPanel");
if (profileInfoPanel) {
    profileInfoPanel.width += 100;
}
console.log(profileInfoPanel); // prints { name: 'ProfileInfoPanel', width: 300, height: 200, children: [] }
