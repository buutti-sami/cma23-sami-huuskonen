// Exercise 1
function factorial(n: number): number {
    if (n <= 0) {
        return 1;
    }
    return n * factorial(n - 1);
}

console.log(factorial(4));


// Exercise 2: Stack overflow
function infinitelyRecurse(n: number) {
    console.log(n);
    infinitelyRecurse(n + 1);
}
// infinitelyRecurse(0);
// Maximum call stack size exceeded at 11266



// Exercise 3: Fibonacci

// function fibonacci(n: number): number {
//     if (n <= 0) {
//         return 0;
//     } else if (n === 1) {
//         return 1;
//     } else {
//         return fibonacci(n - 1) + fibonacci(n - 2);
//     }
// }
// console.log(fibonacci(3));

function fibonacci(n: number, curr = 0, next = 1) {
    if (n === 0) {
        return curr;
    }
    return fibonacci(n - 1, next, curr + next);
}
console.log(fibonacci(3));
console.log(fibonacci(100));