const nameOne = 'Maria';
const nameTwo = 'Philippa';
const nameThree = 'Joe';

const names = [nameOne, nameTwo, nameThree];
names.sort((a, b) => b.length - a.length);
console.log(names);
