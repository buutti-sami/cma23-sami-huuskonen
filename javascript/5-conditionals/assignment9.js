const computerOne = {
    jobTime: 42,
    avgEnergyConsumption: 600
}

const computerTwo = {
    jobTime: 57,
    avgEnergyConsumption: 480
}

const computerOneTotalEnergyConsumption = computerOne.avgEnergyConsumption * computerOne.jobTime;
const computerTwoTotalEnergyConsumption = computerTwo.avgEnergyConsumption * computerTwo.jobTime;


console.log(`Computer number ${computerOneTotalEnergyConsumption > computerTwoTotalEnergyConsumption ? '2' : '1'} used less energy`);


