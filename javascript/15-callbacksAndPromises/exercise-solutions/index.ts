// Exercise 1: countdown to start (callbacks)
function execExerciseOne() {
    function countDown(time: number, callback: () => void) {
        setTimeout(() => {
            callback();
        }, time);
    }

    countDown(1000, () => {
        console.log('3');
        countDown(1000, () => {
            console.log('..2');
            countDown(1000, () => {
                console.log('..1');
                countDown(1000, () => {
                    console.log('GO!');
                });
            });
        });
    });
}

// execExerciseOne();

// // Exercise 2: countdown to start (Promises)
function execExerciseTwo() {
    function countDown(time: number, message: string) {
        return new Promise<void>((resolve) => {
            setTimeout(() => {
                console.log(message);
                resolve();
            }, time);
        });
    }

    countDown(1000, '3')
        .then(() => countDown(1000, '..2'))
        .then(() => countDown(1000, '..1'))
        .then(() => countDown(1000, 'GO!'))
        .catch((error) => {
            console.log(error);
        });
}

// execExerciseTwo();

// Exercise 3: async
async function execExerciseThreeAsync() {
    const getValue = function (): Promise<{ value: number }> {
        return new Promise((res, rej) => {
            setTimeout(() => {
                res({ value: Math.random() });
            }, Math.random() * 1500);
        });
    };

    const valueOne = await getValue();
    const valueTwo = await getValue();

    console.log(
        `Value 1 is ${valueOne.value} and value 2 is ${valueTwo.value}`
    );
}

// execExerciseThreeAsync();

async function execExerciseThreeThen() {
    interface Value {
        value: number;
    }
    const getValue = (): Promise<Value> => {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve({ value: Math.random() });
            }, Math.random() * 1500);
        });
    };

    let valueOne: Value;
    let valueTwo: Value;

    getValue()
        .then((valueObj) => {
            valueOne = valueObj;
            console.log(`Value 1 is ${valueOne.value}`);
            return getValue();
        })
        .then((valueObj) => {
            valueTwo = valueObj;
            console.log(` and value 2 is ${valueTwo.value}`);
        })
        .catch((err) => console.log(err));
}

execExerciseThreeThen();
