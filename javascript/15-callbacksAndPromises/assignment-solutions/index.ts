// Assignment 1 (Promises and delayed execution)
// a)
function execAssignmentOne() {
    function sum(limit: number): number {
        return Array.from({ length: limit + 1 }, (_, i) => i).reduce(
            (acc, curr) => {
                acc += curr;
                return acc;
            }
        );
    }

    console.log(sum(10));

    // b)
    const promise = new Promise((res) => {
        return res(sum(50000));
    });

    console.log(promise);

    // c)
    function createDelayedCalculation(
        limit: number,
        delay: number
    ): Promise<number> {
        return new Promise((res) => {
            setTimeout(() => {
                res(sum(limit));
            }, delay);
        });
    }

    // Prints 200000010000000 after a delay of 2 seconds
    createDelayedCalculation(20000000, 2000).then((result) =>
        console.log(result)
    );

    // Prints 1250025000 after a delay of 0.5 seconds
    createDelayedCalculation(50000, 500).then((result) => console.log(result));

    // e)
    /*While JS is primarily synchronous, it has asynchronous capabilities like with Promises and timers.
    Here JS executes the first createDelayedCalculation function call which returns a Promise that resolves after 2seconds.
    Instead of waiting for this asynchronous operation to finish, JS starts executing the next line of code.
    JS then executes the next createDelayedCalculation function call that also returns a Promise that will resolve after 0.5 seconds.
    Because the second call resolves before the first one, it get's logged before the first. 
    */
}

// execAssignmentOne();

// Assignment 2 (Count seconds)
// a)
function execAssignmentTwo() {
    async function waitFor(delay: number): Promise<void> {
        return new Promise((res, rej) => {
            setTimeout(() => {
                res();
            }, delay);
        });
    }

    // b)
    async function countSeconds() {
        const numbers: number[] = Array.from({ length: 11 }, (_, i) => i);
        for (const n of numbers) {
            console.log(n);
            await waitFor(1000);
        }
    }

    countSeconds();
}

// execAssignmentTwo();

// Assignment 3 (Fetching universities)
async function execAssignmentThree() {
    interface University {
        web_pages: string[];
        alpha_two_code: string;
        country: string;
        name: string;
        'state-province': null | string;
        domains: string[];
    }

    async function getUniversities(): Promise<University[]> {
        try {
            const response = await fetch(
                'http://universities.hipolabs.com/search?country=Finland'
            );
            if (!response.ok) {
                throw new Error('Failed to fetch universities');
            }

            const universities = await response.json();
            return universities;
        } catch (error) {
            console.log(error);
            return [];
        }
    }

    const universityNames = (await getUniversities()).map((uni) => uni.name);
    console.log(universityNames);
}

// execAssignmentThree();

// Assignment 4 (Fake Store API)
function execAssignmentFour() {
    async function getFakeStoreProducts() {
        interface Product {
            id: number;
            title: string;
            price: number;
            description: string;
            category: string;
            image: string;
            rating: { rate: number; count: number };
        }
        try {
            const response = await fetch('https://fakestoreapi.com/products');
            const productNames = (await response.json()).map(
                (p: Product) => p.title
            );
            console.log(productNames);
        } catch (error) {
            console.log(error);
        }
    }

    // a)
    getFakeStoreProducts();
    // prints:
    // Mens Casual Premium Slim Fit T-Shirts
    // Mens Cotton Jacket
    // Mens Casual Slim Fit
    // John Hardy Women's Legends Naga Gold & Silver Dragon Station Chain Bracelet
    // Solid Gold Petite Micropave
    // White Gold Plated Princess
    // ...
    // etc

    // b)
    async function addFakeStoreProduct(
        name: string,
        price: number,
        description: string,
        category: string
    ) {
        try {
            const response = await fetch('https://fakestoreapi.com/products', {
                method: 'POST',
                body: JSON.stringify({
                    title: name,
                    price,
                    description,
                    category,
                }),
            });
            const addedProduct = await response.json();
            console.log(addedProduct);
        } catch (error) {
            console.log(error);
        }
    }

    addFakeStoreProduct(
        'Ibanez RG1527',
        999,
        'A solid electric guitar with 7-strings',
        'electric guitars'
    );

    // c)
    async function deleteFakeStoreProduct(productId: number) {
        const response = await fetch(
            `https://fakestoreapi.com/products/${productId}`,
            { method: 'DELETE' }
        );
        const deletedProduct = await response.json();
        console.log(deletedProduct);
    }

    deleteFakeStoreProduct(20);
}

// execAssignmentFour();
