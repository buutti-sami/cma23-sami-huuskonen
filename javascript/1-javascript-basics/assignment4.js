let number = 5;
const increase = 4;
const limit = 11;

function printNumberIfBiggerThanLimit(aNum) {
    if (aNum > limit) {
        console.log(aNum);
    }
}

number += increase
printNumberIfBiggerThanLimit(number);
number += increase;
printNumberIfBiggerThanLimit(number);
