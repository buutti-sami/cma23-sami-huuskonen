function checkSentenceVowels(sentence) {

    // Check how many different vowels we have in the sentence

    let countOfAs = 0;
    let countOfEs = 0;
    let countOfIs = 0;
    let countOfOs = 0;
    let countOfUs = 0;
    let countOfYs = 0;

    const checkAndCountVowel = (char) => {
        switch (char) {
            case 'a':
                countOfAs++
                break;
            case 'e':
                countOfEs++
                break;
            case 'i':
                countOfIs++
                break;
            case 'o':
                countOfOs++
                break;
            case 'u':
                countOfUs++
                break;
            case 'y':
                countOfYs++
                break;
        
            default:
                break;
        }
    }



    for (let i = 0; i < sentence.length; i++) {
        checkAndCountVowel(sentence.charAt(i).toLowerCase())
    }

    console.log("A letter count: " + countOfAs);
    console.log("E letter count: " + countOfEs);
    console.log("I letter count: " + countOfIs);
    console.log("O letter count: " + countOfOs);
    console.log("U letter count: " + countOfUs);
    console.log("Y letter count: " + countOfYs);

    const totalCount = countOfAs + countOfEs + countOfIs + 
        countOfOs + countOfUs + countOfYs;

    console.log("Total vowel count: " + totalCount);
}

checkSentenceVowels("A wizard's job is to vex chumps quickly in fog.");