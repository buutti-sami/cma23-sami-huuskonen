function exponentValueList(n, numberToMultiply) {
    if (n <= 0) {
        return console.log("n needs to be positive");
    }

    let i = 1;

    while (i <= n) {
        console.log(numberToMultiply ** i );
        i++;
    }
}

exponentValueList(4, 2)