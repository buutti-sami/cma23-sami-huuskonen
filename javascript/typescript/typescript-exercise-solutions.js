// Exercise 1
console.log("Hello World!");
// Exercise 2
function insertNumber(arr, num) {
    arr.push(num);
    arr.sort(function (a, b) { return a - b; });
    return arr;
}
var array = [1, 3, 4, 7, 11];
insertNumber(array, 8);
console.log(array); // prints [ 1, 3, 4, 7, 8, 11 ] 
insertNumber(array, 90);
console.log(array); // prints [ 1, 3, 4, 7, 8, 11, 90 ]
