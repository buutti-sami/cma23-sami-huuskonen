import fs from 'fs';

// Exercise 1
console.log('Hello World!');

// Exercise 2
function insertNumber(arr: number[], num: number) {
    arr.push(num);
    arr.sort((a, b) => a - b);
    return arr;
}

const array = [1, 3, 4, 7, 11];
insertNumber(array, 8);
console.log(array); // prints [ 1, 3, 4, 7, 8, 11 ]
insertNumber(array, 90);
console.log(array); // prints [ 1, 3, 4, 7, 8, 11, 90 ]

// Exercise 3 Primitives
interface Info {
    length: number;
    words: number;
}
const exThreeFunc = (str: string): Info => {
    const info: Info = {
        length: str.length,
        words: str.split(' ').length,
    };
    return info;
};

// Exercise 3 Arrays
const grades: Array<string | number> = [1, '2', 3, '4', 5];
const gradesMoreThanThree = grades.filter((grade) => Number(grade) > 3);
console.log(gradesMoreThanThree);

// Exercise4: Arrays
const createArrayWithRandomNumbers = (size: number) => {
    let randomNums = [];
    for (let i = 0; i < size; i++) {
        randomNums.push(Math.floor(Math.random() * 100));
    }
    return randomNums;
};
const randomNumsAscending = createArrayWithRandomNumbers(10).sort(
    (a, b) => a - b
);
const randomNumsDescending = createArrayWithRandomNumbers(5).sort(
    (a, b) => b - a
);

console.log('randomNumsAscending', randomNumsAscending);
console.log('randomNumsDescending', randomNumsDescending);

// Exercise: Extra 1
const str = 'adsf qwerty';
const asciiFromStr = (str: string) => {
    let asciiCodes = [];
    for (let i = 0; i < str.length; i++) {
        asciiCodes.push(str.charCodeAt(i));
    }
    return asciiCodes;
};
console.log(asciiFromStr(str));

// Exercise: Extra 2
const findSingleNumbers = (str: string) => {
    let numbers = [];

    for (let i = 0; i < str.length; i++) {
        const char = str[i];

        if (!isNaN(parseFloat(char))) {
            numbers.push(char);
        }
    }
    const sum = numbers.reduce((acc, curr) => {
        acc += curr;
        return acc;
    });
    return sum;
};
console.log(findSingleNumbers('Dslfelka21kjnoi350983n2n1292m12lkl2kj4'));

// Exercise 5: Objects
type Ran<T extends number> = number extends T ? number : _Range<T, []>;
type _Range<T extends number, R extends unknown[]> = R['length'] extends T
    ? R[number]
    : _Range<T, [R['length'], ...R]>;

type FillLevel = Ran<101>;

interface Boat {
    hullBreached: boolean;
    fillLevel: FillLevel;
    isItSinking: () => void;
    sinked?: boolean;
}

const boat: Boat = {
    hullBreached: true,
    fillLevel: 10,
    isItSinking: function () {
        if (this.hullBreached) {
            const full = 100;
            while (this.fillLevel < full) {
                const fillIncrement = 20;
                this.fillLevel += fillIncrement;
                if (this.fillLevel >= 100) {
                    console.log(`Hull breached! Fill level is at 100%`);
                } else {
                    console.log(
                        `Hull breached! Fill level is at ${this.fillLevel}%`
                    );
                }
            }
            this['sinked'] = true;
        }
    },
};

boat.isItSinking();
console.log(boat);

// Exercise 6: Objects and JSON
interface Book {
    author: string;
    title: string;
    readingStatus: boolean;
    id: number;
}
const library = [
    {
        author: 'David Wallace',
        title: 'Infinite Jest',
        readingStatus: false,
        id: 1,
    },
    {
        author: 'Douglas Hofstadter',
        title: 'Gödel, Escher, Bach',
        readingStatus: true,
        id: 2,
    },
    {
        author: 'Harper Lee',
        title: 'To Kill A Mockingbird',
        readingStatus: false,
        id: 3,
    },
];

function getBook(id: number) {
    return library.find((book) => book.id === id);
}

function printBookData(id: number) {
    console.log(getBook(id));
}

function printReadingStatus(author: string, title: string) {
    const book = library.find(
        (book) => book.author === author && book.title === title
    );
    if (book) {
        console.log(book.readingStatus);
    }
}

function addNewBook(author: string, title: string) {
    const newBook: Book = { author, title, readingStatus: false, id: 68 };
    const updatedLibrary = [...library, newBook];
    return updatedLibrary;
}

function readBook(id: number) {
    const updatedLibrary = library.map((book) => {
        if (book.id !== id) {
            return book;
        } else {
            return { ...book, readingStatus: true };
        }
    });
    return updatedLibrary;
}

function saveToJSON(library: Book[]) {
    const libraryString = JSON.stringify(library, null, 2);

    try {
        fs.writeFileSync('./data/library.json', libraryString);
        console.log('File written successfully');
    } catch (error) {
        console.log(error);
    }
}

// saveToJSON(library);

async function loadFromJSON(path: string) {
    try {
        const library = await fs.readFileSync(path, 'utf-8');
        console.log(JSON.parse(library));
    } catch (error) {
        console.log(error);
    }
}

// loadFromJSON('./data/library.json');

// Exercise 7: type guards
function isNumberArray(elems: unknown[]) {
    for (const e of elems) {
        if (typeof e !== 'number') {
            return false;
        }
    }
    return true;
}

const numArr = [1, 2, 3, 4];
const notNumArr = ['1', 'jee', 99, null];

console.log('is numArr an array of numbers? ', isNumberArray(numArr));
console.log('is notNumArr an array of numbers? ', isNumberArray(notNumArr));
