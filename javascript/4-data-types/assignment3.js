const a = true;
const b = false;

const c = a + b;
const d = 10 + a;
const e = 10 + b;

console.log('c', c);
console.log('d', d);
console.log('e', e);

/*true value is treated as 1 in arithmetic operations while false is treated as 0 */