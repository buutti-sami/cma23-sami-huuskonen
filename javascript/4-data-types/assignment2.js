let number;
const result1 = 10 + number;

number = null;
const result2 = 10 + number;

console.log(result1);
console.log(result2);

/*JavaScript applies ToNumber operation for arithmetic expression and because of that Undefined is treated as NaN and null as 0. 
That is why the other operation works and the other doesn't
*/