let books = [];

function handleBookSubmit(event) {
    event.preventDefault();

    const bookNameInput = document.getElementById('book-name');
    const pageCountInput = document.getElementById('page-count');

    const bookName = bookNameInput.value;
    const bookPageCount = pageCountInput.value;

    books.push({ bookName, pageCount: bookPageCount });

    bookNameInput.value = '';
    pageCountInput.value = '';

    showBooks();
}

function showBooks() {
    const targetEl = document.getElementById('book-list');

    targetEl.innerHTML = '';

    for (const book of books) {
        const div = document.createElement('div');
        div.textContent = `${book.bookName} (${book.pageCount} pages)`;
        targetEl.appendChild(div);
    }
}

window.onload = showBooks;
