const students = [
    { name: 'Sami', score: 24.75 },
    { name: 'Heidi', score: 20.25 },
    { name: 'Jyrki', score: 27.5 },
    { name: 'Helinä', score: 26.0 },
    { name: 'Maria', score: 17.0 },
    { name: 'Yrjö', score: 14.5 },
];

function toggleStudents() {
    const baseEl = document.getElementById('student-table');

    if (baseEl) {
        if (baseEl.hasChildNodes()) {
            while (baseEl.firstChild) {
                baseEl.removeChild(baseEl.firstChild);
            }
            return;
        }

        const tableEl = document.createElement('table');

        const headerTitles = Object.keys(students[0]);
        const tableHeaderRow = document.createElement('tr');

        for (const title of headerTitles) {
            const tableHeaderCell = document.createElement('th');
            tableHeaderCell.textContent = title;
            tableHeaderRow.appendChild(tableHeaderCell);
        }
        tableEl.appendChild(tableHeaderRow);

        for (const student of students) {
            const tableRow = document.createElement('tr');
            const tableCellStudentName = document.createElement('td');
            tableCellStudentName.textContent = student.name;
            tableRow.appendChild(tableCellStudentName);

            const tableCellStudentScore = document.createElement('td');
            tableCellStudentScore.textContent = student.score;
            tableRow.appendChild(tableCellStudentScore);
            tableEl.appendChild(tableRow);
        }
        baseEl.appendChild(tableEl);
    }
}

let seconds = 0;

function showSeconds() {
    const targetEl = document.getElementById('time');
    const hours = Math.floor(seconds / 3600);
    const minutes = Math.floor((seconds % 3600) / 60);
    const remainingSeconds = seconds % 60;

    const hoursStr = hours > 0 ? `${hours} hour${hours > 1 ? 's' : ''}` : '';
    const minutesStr =
        minutes > 0 ? `${minutes} minute${minutes > 1 ? 's' : ''}` : '';
    const secondsStr =
        remainingSeconds > 0
            ? `${remainingSeconds} second${remainingSeconds > 1 ? 's' : ''}`
            : '';

    const timeParts = [hoursStr, minutesStr, secondsStr].filter(
        (part) => part !== ''
    );

    targetEl.textContent = timeParts.join(', ');
}

function incrementSeconds() {
    seconds += 1;
    showSeconds();
}

setInterval(incrementSeconds, 1000);

let posts = [];

function addPost(event) {
    event.preventDefault();

    const nameInputElem = document.getElementById('name');
    const postInputElem = document.getElementById('post');
    const name = nameInputElem.value;
    const post = postInputElem.value;

    const randomId = Math.floor(Math.random() * 10000);

    posts.push({ name, post, id: randomId });
    renderPosts();

    nameInputElem.value = '';
    postInputElem.value = '';
}

function showAddPostForm() {
    const formEl = document.querySelector('form');
    if (formEl) {
        formEl.style.display = 'block';
    }
}

function deletePost(postId) {
    posts = posts.filter((post) => post.id !== postId);
}

function renderPosts() {
    const targetEl = document.getElementById('posts-container');

    targetEl.innerHTML = '';

    for (const post of posts) {
        const postDiv = document.createElement('div');

        const deleteButton = document.createElement('button');
        deleteButton.textContent = 'Delete';
        deleteButton.addEventListener('click', () => {
            deletePost(post.id);
            renderPosts();
        });

        const nameHeader = document.createElement('h2');
        const postParagraph = document.createElement('p');
        nameHeader.textContent = post.name;
        postParagraph.textContent = post.post;

        postDiv.appendChild(nameHeader);
        postDiv.appendChild(postParagraph);
        postDiv.appendChild(deleteButton);

        targetEl.appendChild(postDiv);
    }
}

window.onload = renderPosts();
