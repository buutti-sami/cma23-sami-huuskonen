import { executeQuery } from '../db';
import {
    createUsersTableQuery,
    insertUserQuery,
    getAllUsersQuery,
    getUserByEmailQuery,
} from '../queries';

export interface UserWithoutID {
    email: string;
    passwordHash: string;
    admin: boolean;
}

export const createUsersTable = async () => {
    await executeQuery(createUsersTableQuery);
    console.log('Table users created');
};

export const insertUser = async (
    email: string,
    passwordHash: string,
    admin: boolean
) => {
    const params = [email, passwordHash, admin];
    const result = await executeQuery(insertUserQuery, params);
    console.log(`User ${email} added to users table`);
    return result;
};

export const getAllUsers = async () => {
    const result = await executeQuery(getAllUsersQuery);
    return result;
};

export const getUserByEmail = async (email: string) => {
    const result = await executeQuery(getUserByEmailQuery, [email]);
    return result;
};
