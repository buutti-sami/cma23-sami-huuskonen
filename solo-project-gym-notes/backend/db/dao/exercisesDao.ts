import { executeQuery } from '../db';
import {
    createExercisesTableQuery,
    getAllExercisesQuery,
    getExerciseByIdQuery,
    getExercisesByMuscleGroupIdQuery,
    insertExerciseQuery,
} from '../queries';

export const createExercisesTable = async () => {
    await executeQuery(createExercisesTableQuery);
    console.log('Table exercises created');
};

export const insertExercise = async (
    name: string,
    muscleGroupId: string,
    description: string
) => {
    const params = [name, muscleGroupId, description];
    const result = await executeQuery(insertExerciseQuery, params);
    console.log(`${name} added to exercises table`);
    return result;
};

export const getAllExercises = async () => {
    const result = await executeQuery(getAllExercisesQuery);
    return result;
};

export const getExercisesByMuscleGroupId = async (muscleGroupId: string) => {
    const result = await executeQuery(getExercisesByMuscleGroupIdQuery, [
        muscleGroupId,
    ]);
    return result;
};

export const getExerciseById = async (exerciseId: string) => {
    const result = await executeQuery(getExerciseByIdQuery, [exerciseId]);
    return result;
};
