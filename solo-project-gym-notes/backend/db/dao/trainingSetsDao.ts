import { executeQuery } from '../db';
import {
    createTrainingSetsTableQuery,
    insertTrainingSetQuery,
    getTrainingSetsByUserAndExerciseQuery,
    updateTrainingSetQuery,
    deleteTrainingSetQuery,
} from '../queries';

export const createTrainingSetsTable = async () => {
    await executeQuery(createTrainingSetsTableQuery);
    console.log('Table training_sets created');
};

export const insertTrainingSet = async (
    user_id: string,
    exerciseId: string,
    weight: string,
    reps: string,
    date: string,
    description?: string
) => {
    const result = await executeQuery(insertTrainingSetQuery, [
        user_id,
        exerciseId,
        weight,
        reps,
        date,
        description || '',
    ]);
    console.log(
        `Training set user_id ${user_id}, exercise_id ${exerciseId}, weight ${weight}, reps ${reps}, date ${date}, description ${description} added to database`
    );
    return result;
};

export const getTrainingSetsByUserAndExercise = async (
    userId: string,
    exerciseId: string
) => {
    const result = await executeQuery(getTrainingSetsByUserAndExerciseQuery, [
        userId,
        exerciseId,
    ]);
    return result;
};

export const updateTrainingSet = async (
    trainingSetId: string,
    weight: string,
    reps: string,
    description: string
) => {
    const result = await executeQuery(updateTrainingSetQuery, [
        trainingSetId,
        weight,
        reps,
        description,
    ]);
    return result;
};

export const deleteTrainingSet = async (trainingSetId: string) => {
    const result = await executeQuery(deleteTrainingSetQuery, [trainingSetId]);
    return result;
};
