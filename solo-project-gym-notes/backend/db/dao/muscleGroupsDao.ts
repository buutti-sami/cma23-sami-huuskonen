import { executeQuery } from '../db';
import {
    createMuscleGroupsTableQuery,
    insertMuscleGroupQuery,
    getAllMuscleGroupsQuery,
} from '../queries';

export const createMuscleGroupsTable = async () => {
    await executeQuery(createMuscleGroupsTableQuery);
    console.log('Table muscle_groups created');
};

export const insertMuscleGroup = async (
    muscleGroup: string,
    description: string
) => {
    const params = [muscleGroup, description];
    const result = await executeQuery(insertMuscleGroupQuery, params);
    console.log(`${muscleGroup} added to muscle_groups table`);
    return result;
};

export const getAllMuscleGroups = async () => {
    const result = await executeQuery(getAllMuscleGroupsQuery);
    return result;
};
