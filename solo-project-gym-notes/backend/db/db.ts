import pg from 'pg';
import dotenv from 'dotenv';

dotenv.config();

const { PG_USER, PG_PASSWORD, PG_DATABASE, PG_PORT, PG_HOST } = process.env;

const pool = new pg.Pool({
    user: PG_USER,
    password: PG_PASSWORD,
    port: Number(PG_PORT),
    host: PG_HOST,
    database: PG_DATABASE,
});

pg.types.setTypeParser(1082, function (stringValue) {
    return stringValue;
});

export const executeQuery = async (
    query: string,
    parameters?: Array<string | boolean>
) => {
    try {
        const client = await pool.connect();
        try {
            const result = await client.query(query, parameters);
            return result;
        } catch (error: unknown) {
            if (error instanceof Error) {
                console.error(error.stack);
                error.name = 'dbError';
                throw error;
            }
        } finally {
            client.release();
        }
    } catch (error: unknown) {
        if (error instanceof Error) {
            error.name = 'dbError';
            throw error;
        }
    }
};
