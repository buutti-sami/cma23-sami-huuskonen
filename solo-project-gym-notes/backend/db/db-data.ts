interface MuscleGroup {
    name: string;
    description: string;
}

export const muscleGroups: MuscleGroup[] = [
    {
        name: 'Chest',
        description:
            'The chest muscles, or pectorals, are responsible for pushing movements, like bench presses, and give your upper body its strength and definition.',
    },
    {
        name: 'Back',
        description:
            'The back muscles, including the lats and traps, are crucial for pulling exercises and maintaining good posture, enhancing overall upper body strength.',
    },
    {
        name: 'Legs',
        description:
            'The leg muscles, such as quads, hamstrings, and calves, provide the foundation for lower body strength and support a wide range of movements like squats and lunges.',
    },
    {
        name: 'Shoulders',
        description:
            'The deltoid muscles help create broad, powerful shoulders, and enable you to perform overhead presses and raise your arms in various directions.',
    },
    {
        name: 'Biceps',
        description:
            'The biceps are the front arm muscles responsible for flexing the elbow joint and are essential for exercises like curls and pull-ups.',
    },
    {
        name: 'Triceps',
        description:
            'The triceps, located on the back of the upper arm, are crucial for extending the elbow and support pushing movements like tricep dips and bench presses.',
    },
    {
        name: 'Abs',
        description:
            'The abdominal muscles, or core, provide stability, balance, and support for nearly all movements, including planks and crunches, while contributing to a defined midsection.',
    },
];

export interface Exercise {
    name: string;
    muscleGroupId: string;
    description: string;
}

export const exercises: Exercise[] = [
    // Chest
    {
        muscleGroupId: '1',
        name: 'Bench Press',
        description: 'Lay on a bench and press a barbell or dumbbells upward.',
    },
    {
        muscleGroupId: '1',
        name: 'Push-Ups',
        description:
            'Support your body on your hands and toes while lowering and raising your chest.',
    },
    {
        muscleGroupId: '1',
        name: 'Dumbbell Flyes',
        description:
            'Lie on a bench and extend your arms out to the sides with dumbbells in hand.',
    },
    {
        muscleGroupId: '1',
        name: 'Incline Bench Press',
        description:
            'Perform bench press on an inclined bench to target the upper chest.',
    },
    {
        muscleGroupId: '1',
        name: 'Cable Crossovers',
        description: 'Use cables to simulate a chest fly motion for isolation.',
    },

    // Back
    {
        muscleGroupId: '2',
        name: 'Straight Arm Pull Down',
        description:
            'Pull a bar attached to a cable down with straight arms using your back muscles.',
    },
    {
        muscleGroupId: '2',
        name: 'Pull-Ups',
        description: 'Hang from a bar and pull your body up to it.',
    },
    {
        muscleGroupId: '2',
        name: 'Bent Over Rows',
        description:
            'Bend at the waist and row a barbell or dumbbells to your lower ribcage.',
    },
    {
        muscleGroupId: '2',
        name: 'Lat Pulldowns',
        description: 'Use a cable machine to pull a bar down to your chest.',
    },
    {
        muscleGroupId: '2',
        name: 'T-Bar Rows',
        description: 'Row a weighted barbell with a T-bar attachment.',
    },

    // Legs
    {
        muscleGroupId: '3',
        name: 'Squats',
        description:
            'Bend at your knees and hips to lower your body, then rise back up.',
    },
    {
        muscleGroupId: '3',
        name: 'Lunges',
        description:
            'Step forward with one leg and lower your body until both knees are bent at 90 degrees.',
    },
    {
        muscleGroupId: '3',
        name: 'Leg Press',
        description: 'Press a weighted platform with your feet while seated.',
    },
    {
        muscleGroupId: '3',
        name: 'Deadlift',
        description: 'Lift a barbell from the ground to a standing position.',
    },
    {
        muscleGroupId: '3',
        name: 'Calf Raises',
        description:
            'Stand on your toes with weights to work the calf muscles.',
    },

    // Shoulders
    {
        muscleGroupId: '4',
        name: 'Military Press',
        description:
            'Press a barbell or dumbbells overhead while standing or seated.',
    },
    {
        muscleGroupId: '4',
        name: 'Lateral Raises',
        description:
            'Raise dumbbells to the sides while standing to target the lateral deltoids.',
    },
    {
        muscleGroupId: '4',
        name: 'Front Raises',
        description:
            'Lift dumbbells in front of you to work the front deltoids.',
    },
    {
        muscleGroupId: '4',
        name: 'Upright Rows',
        description:
            'Lift a barbell or dumbbells close to your body to target the traps and shoulders.',
    },
    {
        muscleGroupId: '4',
        name: 'Face Pulls',
        description:
            'Pull a rope attachment toward your face to work the rear deltoids.',
    },

    // Biceps
    {
        muscleGroupId: '5',
        name: 'Barbell Curls',
        description: 'Curl a barbell upward while standing.',
    },
    {
        muscleGroupId: '5',
        name: 'Dumbbell Curls',
        description: 'Curl dumbbells upward while standing or seated.',
    },
    {
        muscleGroupId: '5',
        name: 'Hammer Curls',
        description:
            'Curl dumbbells with a neutral grip to target the brachialis.',
    },
    {
        muscleGroupId: '5',
        name: 'Preacher Curls',
        description:
            'Curl a barbell or dumbbells using a preacher curl bench for isolation.',
    },
    {
        muscleGroupId: '5',
        name: 'Chin-Ups',
        description:
            'Pull your body up to a bar with your palms facing you to work the biceps.',
    },

    // Triceps
    {
        muscleGroupId: '6',
        name: 'Tricep Dips',
        description:
            'Dip down and up using parallel bars to target the triceps.',
    },
    {
        muscleGroupId: '6',
        name: 'Tricep Pushdowns',
        description:
            'Use a cable machine to push a bar down to extend the triceps.',
    },
    {
        muscleGroupId: '6',
        name: 'Skull Crushers',
        description:
            'Lift a barbell or dumbbells above your head, then bend your elbows to lower it toward your forehead.',
    },
    {
        muscleGroupId: '6',
        name: 'Close-Grip Bench Press',
        description:
            'Perform bench press with a narrow grip to emphasize the triceps.',
    },
    {
        muscleGroupId: '6',
        name: 'Tricep Kickbacks',
        description: 'Extend your forearm back using dumbbells for isolation.',
    },

    // Abs
    {
        muscleGroupId: '7',
        name: 'Crunches',
        description:
            'Lift your upper body off the ground by contracting the abdominal muscles.',
    },
    {
        muscleGroupId: '7',
        name: 'Leg Raises',
        description:
            'Raise your legs upward from a supine position to engage the lower abs.',
    },
    {
        muscleGroupId: '7',
        name: 'Planks',
        description:
            'Support your body on your forearms and toes in a straight line to engage the core.',
    },
    {
        muscleGroupId: '7',
        name: 'Russian Twists',
        description:
            'Sit on the ground, lean back slightly, and twist your torso to work the obliques.',
    },
    {
        muscleGroupId: '7',
        name: 'Hanging Leg Raises',
        description:
            'Hang from a bar and raise your legs to work the lower abs.',
    },
];
