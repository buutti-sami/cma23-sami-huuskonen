export const createUsersTableQuery = `
CREATE TABLE IF NOT EXISTS "users" (
    "id" SERIAL PRIMARY KEY,
    "email" VARCHAR(100) UNIQUE NOT NULL,
    "password_hash" VARCHAR(100) NOT NULL,
    "admin" boolean NOT NULL
)
`;

export const createMuscleGroupsTableQuery = `
CREATE TABLE IF NOT EXISTS "muscle_groups" (
    "id" SERIAL PRIMARY KEY,
    "name" VARCHAR(50) UNIQUE NOT NULL,
    "description" VARCHAR(200) NOT NULL
)
`;

export const createExercisesTableQuery = `
CREATE TABLE IF NOT EXISTS "exercises" (
    "id" SERIAL PRIMARY KEY,
    "name" VARCHAR(100) UNIQUE NOT NULL,
    "muscle_group_id" integer NOT NULL, 
    "description" VARCHAR(200) NOT NULL,
    FOREIGN KEY (muscle_group_id) REFERENCES muscle_groups (id)
)
`;

export const createTrainingSetsTableQuery = `
CREATE TABLE IF NOT EXISTS "training_sets" (
    "id" SERIAL PRIMARY KEY,
    "user_id" integer NOT NULL,
    "exercise_id" integer NOT NULL,
    "weight" real NOT NULL,
    "reps" integer NOT NULL,
    "date" date NOT NULL,
    "description" VARCHAR(200) NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users (id),
    FOREIGN KEY (exercise_id) REFERENCES exercises (id)
)
`;

export const insertTrainingSetQuery = `
INSERT INTO training_sets (user_id, exercise_id, weight, reps, date, description)
VALUES ($1, $2, $3, $4, $5, $6) RETURNING id
`;

export const getTrainingSetsByUserAndExerciseQuery = `
SELECT * FROM training_sets WHERE user_id = $1 AND exercise_id = $2
`;

export const updateTrainingSetQuery = `
UPDATE training_sets SET weight = $2, reps = $3, description = $4 WHERE id = $1
`;

export const deleteTrainingSetQuery = `
DELETE FROM training_sets WHERE id = $1 
`;

export const insertExerciseQuery = `
INSERT INTO exercises (name, muscle_group_id, description)
VALUES ($1, $2, $3)
`;

export const getAllExercisesQuery = `
SELECT * FROM exercises
`;

export const getExercisesByMuscleGroupIdQuery = `
SELECT * FROM exercises WHERE muscle_group_id = $1
`;

export const getExerciseByIdQuery = `
SELECT * FROM exercises WHERE id = $1
`;

export const insertMuscleGroupQuery = `
INSERT INTO muscle_groups (name, description)
VALUES ($1, $2)
`;

export const getAllMuscleGroupsQuery = `
SELECT * FROM muscle_groups
`;

export const insertUserQuery = `
INSERT INTO users (
    email, password_hash, admin
) VALUES (
    $1, $2, $3
) RETURNING id
`;

export const getAllUsersQuery = `
SELECT * FROM users
`;

export const getUserByEmailQuery = `
SELECT * FROM users WHERE email = $1
`;
