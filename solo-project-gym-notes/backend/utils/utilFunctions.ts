import OpenAI from 'openai';
import fs from 'fs';

import {
    createExercisesTable,
    createMuscleGroupsTable,
    createTrainingSetsTable,
    createUsersTable,
    getAllExercises,
    getAllMuscleGroups,
    insertExercise,
    insertMuscleGroup,
} from '../db/dao';

import { exercises, muscleGroups } from '../db/db-data';
import { writeImageToDisk } from './writeImageToDisk';

const openai = new OpenAI();
export const imagesPath = './public/images';

export const insertMuscleGroupsToTable = async () => {
    const muscleGroupsInDb = await getAllMuscleGroups();
    if (muscleGroupsInDb?.rowCount === 0) {
        for (const muscleGroup of muscleGroups) {
            await insertMuscleGroup(muscleGroup.name, muscleGroup.description);
        }
    } else {
        console.log(
            'muscle_groups table already filled with muscle groups, skip inserting muscle groups to table...'
        );
    }
};

export const insertExercisesToTable = async () => {
    const exercisesInDb = await getAllExercises();

    if (exercisesInDb?.rowCount === 0) {
        for (const exercise of exercises) {
            await insertExercise(
                exercise.name,
                exercise.muscleGroupId,
                exercise.description
            );
        }
    } else {
        console.log(
            'exercises table already filled with exercises, skip inserting exercises again'
        );
    }
};

const createImage = async (prompt: string) => {
    try {
        const completionsResponse = await openai.chat.completions.create({
            model: 'gpt-3.5-turbo',
            messages: [
                {
                    role: 'user',
                    content: `make the following prompt (Prompt: {prompt}) more detailed and better for AI-image generation model in context of gym, exercise and weight lifting. Be as concise as you can. Image should be simple and clear and should not contain any text. Prompt: ${prompt}`,
                },
            ],
        });

        const enhancedPrompt = completionsResponse.choices[0].message.content;

        console.log('enhancedPrompt', enhancedPrompt);
        const image = await openai.images.generate({
            prompt: enhancedPrompt || prompt,
            size: '256x256',
            n: 1,
        });

        console.log('image.data :', image.data);

        return image.data[0].url;
    } catch (error) {
        console.log(error);
    }
};

interface ExerciseType {
    id: string;
    name: string;
    muscleGroupId: string;
    description: string;
}

const createSaveImagesForExercises = async () => {
    try {
        const allExercisesResult = await getAllExercises();

        const exercises = allExercisesResult?.rows as ExerciseType[];

        if (!exercises || exercises.length < 1) {
            throw new Error('No exercises found');
        }

        const createdImageNames = fs
            .readdirSync(imagesPath)
            .map((imgName: string) => {
                let formattedImgName = imgName.replace(/.png/, '');
                formattedImgName = formattedImgName.replace(/_/g, ' ');
                return formattedImgName;
            });

        for (const exercise of exercises) {
            if (
                !createdImageNames.find(
                    (imgName) => imgName === exercise.name.toLowerCase()
                )
            ) {
                const prompt = `outline drawing of a single person doing ${exercise.name}`;
                const imageUrl = await createImage(prompt);

                if (!imageUrl) {
                    throw new Error('No url for AI image');
                }

                await writeImageToDisk(
                    imageUrl,
                    `${exercise.name.toLocaleLowerCase()}.png`
                );
            }
        }
    } catch (error) {
        console.log(error);
    }
};

interface MuscleGroupType {
    name: string;
    description: string;
    id: number | string;
}

const createSaveImagesForMuscleGroups = async () => {
    try {
        const allMuscleGroupsResult = await getAllMuscleGroups();

        const muscleGroups = allMuscleGroupsResult?.rows as MuscleGroupType[];

        if (!muscleGroups || muscleGroups.length < 1) {
            throw new Error('No muscle groups found');
        }

        const createdImageNames = fs
            .readdirSync(imagesPath)
            .map((imgName: string) => {
                let formattedImgName = imgName.replace(/.png/, '');
                formattedImgName = formattedImgName.replace(/_/g, ' ');
                return formattedImgName;
            });

        for (const muscleGroup of muscleGroups) {
            if (
                !createdImageNames.find(
                    (imgName) => imgName === muscleGroup.name.toLowerCase()
                )
            ) {
                const prompt = `anatomical style drawing of muscle group ${muscleGroup.name} in context of and position in human body`;
                const imageUrl = await createImage(prompt);

                if (!imageUrl) {
                    throw new Error('No url for AI image');
                }

                await writeImageToDisk(
                    imageUrl,
                    `${muscleGroup.name.toLocaleLowerCase()}.png`
                );
            }
        }
    } catch (error) {
        console.log(error);
    }
};

export const createTablesAndFillWithData = async () => {
    try {
        await createUsersTable();
        await createMuscleGroupsTable();
        await createExercisesTable();
        await createTrainingSetsTable();

        await insertMuscleGroupsToTable();
        await insertExercisesToTable();

        await createSaveImagesForMuscleGroups();
        await createSaveImagesForExercises();
    } catch (error) {
        console.log(error);
    }
};
