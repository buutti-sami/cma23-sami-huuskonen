import https from 'https';
import fs from 'fs';
import path from 'path';
import { imagesPath } from './utilFunctions';

export const writeImageToDisk = async (imageURL: string, fileName: string) => {
    try {
        // Create directory if it doesn't exist
        if (!fs.existsSync(imagesPath)) {
            fs.mkdirSync(imagesPath, { recursive: true });
        }

        const sanitizedFileName = fileName.replace(/ /g, '_');

        const fileStream = fs.createWriteStream(
            path.join(imagesPath, sanitizedFileName)
        );

        https
            .get(imageURL, (response) => {
                if (response.statusCode === 200) {
                    response.pipe(fileStream);

                    fileStream.on('finish', () => {
                        fileStream.close(() => {
                            console.log(
                                `Image ${fileName} saved to ${imagesPath}`
                            );
                        });
                    });
                } else {
                    console.error(
                        `Failed to download image. Status code: ${response.statusCode}`
                    );
                }
            })
            .on('error', (err) => {
                console.error(err);
            });
    } catch (error) {
        console.log(error);
    }
};
