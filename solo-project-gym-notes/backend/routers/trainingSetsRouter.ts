import { Router, Request, Response } from 'express';
import { handleError, sendSuccess } from './usersRouter';
import {
    getTrainingSetsByUserAndExercise,
    insertTrainingSet,
    updateTrainingSet,
    deleteTrainingSet,
} from '../db/dao';

const router = Router();

router.get('/training-sets/:id', async (req: Request, res: Response) => {
    try {
        const userId = req.query.userId as string;
        const { id } = req.params;

        if (!userId || !id) {
            return handleError(
                res,
                new Error('user id (userId) or exercise id (id) missing')
            );
        }

        const result = await getTrainingSetsByUserAndExercise(userId, id);

        if (result) {
            const exercises = result.rows;
            sendSuccess(res, exercises);
        }
    } catch (error) {
        if (error instanceof Error) {
            handleError(res, error);
        }
    }
});

router.post('/training-sets', async (req: Request, res: Response) => {
    try {
        const { exerciseId, userId, weight, reps, date, description } =
            req.body;

        if (!exerciseId || !userId || !weight || !reps || !date) {
            return handleError(res, new Error('Invalid request body'));
        }

        const result = await insertTrainingSet(
            userId,
            exerciseId,
            weight,
            reps,
            date,
            description
        );
        if (result) {
            const trainingSetId = result.rows[0].id;
            sendSuccess(res, trainingSetId);
        }
    } catch (error) {
        if (error instanceof Error) {
            handleError(res, error);
        }
    }
});

router.patch('/training-sets/:id', async (req: Request, res: Response) => {
    try {
        const { id: trainingSetId } = req.params;
        const { weight, reps, description } = req.body;

        if (!trainingSetId || !weight || !reps || !description) {
            return handleError(
                res,
                new Error('Invalid request body or params')
            );
        }

        const result = await updateTrainingSet(
            trainingSetId,
            weight,
            reps,
            description
        );

        if (result && result.rowCount !== 0) {
            sendSuccess(res, 'Successful update');
        } else {
            handleError(res, new Error('Something went wrong'));
        }
    } catch (error) {
        if (error instanceof Error) {
            handleError(res, error);
        }
    }
});

router.delete('/training-sets/:id', async (req: Request, res: Response) => {
    const { id: trainingSetId } = req.params;

    if (!trainingSetId) {
        return handleError(
            res,
            new Error('training set id missing from req params')
        );
    }

    const result = await deleteTrainingSet(trainingSetId);

    if (result && result.rowCount !== 0) {
        console.log('result', result);
        sendSuccess(res, 'Successful deletion');
    } else {
        handleError(res, new Error('Something went wrong'));
    }
});

export default router;
