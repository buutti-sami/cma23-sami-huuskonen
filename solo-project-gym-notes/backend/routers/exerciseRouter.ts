import { Router, Request, Response } from 'express';
import {
    getAllExercises,
    getExerciseById,
    getExercisesByMuscleGroupId,
} from '../db/dao';
import { handleError, sendSuccess } from './usersRouter';

const router = Router();

router.get('/exercises', async (req: Request, res: Response) => {
    try {
        const muscleGroupId = req.query.muscleGroupId as string;

        const exercisesResult = muscleGroupId
            ? await getExercisesByMuscleGroupId(muscleGroupId)
            : await getAllExercises();

        if (!exercisesResult || exercisesResult?.rowCount === 0) {
            return handleError(res, new Error(`Exercises not found`));
        }

        const exercises = exercisesResult.rows;
        const exercisesNameIdMapping = exercisesResult.rows.reduce(
            (obj, curr) => {
                obj[curr.name.toLowerCase()] = curr.id;
                return obj;
            },
            {}
        );

        sendSuccess(res, { exercises, exercisesNameIdMapping });
    } catch (error) {
        if (error instanceof Error) {
            handleError(res, error);
        }
    }
});

router.get('/exercises/:id', async (req: Request, res: Response) => {
    const { id } = req.params;

    if (!id) {
        return handleError(res, new Error('Exercise id missing from params'));
    }

    try {
        const result = await getExerciseById(id);

        if (result) {
            if (result.rowCount === 0) {
                return handleError(
                    res,
                    new Error(`Exercise by id: ${id} not found`)
                );
            }

            const exercise = result.rows[0];
            sendSuccess(res, exercise);
        }
    } catch (error) {
        if (error instanceof Error) {
            handleError(res, error);
        }
    }
});

export default router;
