import { Router, Request, Response } from 'express';
import { getAllMuscleGroups } from '../db/dao';
import { handleError, sendSuccess } from './usersRouter';

const router = Router();

router.get('/muscle-groups', async (_req: Request, res: Response) => {
    try {
        const allMuscleGroupsResult = await getAllMuscleGroups();

        if (allMuscleGroupsResult) {
            const muscleGroups = allMuscleGroupsResult.rows;
            const muscleGroupsNameIdMapping = allMuscleGroupsResult.rows.reduce(
                (obj, curr) => {
                    obj[curr.name.toLowerCase()] = curr.id;
                    return obj;
                },
                {}
            );
            return sendSuccess(res, {
                muscleGroups,
                muscleGroupsNameIdMapping,
            });
        } else {
            handleError(res, new Error('Muscle groups not found'));
        }
    } catch (error) {
        if (error instanceof Error) {
            handleError(res, error);
        }
    }
});

export default router;
