import { Router, Response } from 'express';
import argon2 from 'argon2';
import jwt from 'jsonwebtoken';

import { getUserByEmail, insertUser } from '../db/dao';
import { DatabaseError } from 'pg';

const router = Router();

const JWT_SECRET = process.env.JWT_SECRET as string;

export interface User {
    id: string | number;
    email: string;
    password_hash: string;
    admin: boolean;
}

const TOKEN_COOKIE_EXP = 60000 * 60 * 24;

const createToken = (email: string) => {
    return jwt.sign({ email }, JWT_SECRET, {
        expiresIn: TOKEN_COOKIE_EXP,
    });
};

const validateEmailAndPassword = (email: string, password: string): boolean => {
    console.log('email + pass ', email + ' ' + password);
    const isValidEmail = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(email);

    console.log('isValidEmail', isValidEmail);
    const isPasswordValid = password.length >= 8;

    console.log('isPasswordValid', isPasswordValid);

    return !!email && !!password && isValidEmail && isPasswordValid;
};

export const sendSuccess = (res: Response, data: unknown) => {
    return res.status(200).json(data);
};

export const handleError = (res: Response, error: Error) => {
    console.log('error: ', error);
    const errorResponse = { error: error.message };
    res.status(400).json(errorResponse);
};

router.post('/signup', async (req, res) => {
    const { email, password } = req.body;

    console.log(
        'validateEmailAndPassword(email, password)',
        validateEmailAndPassword(email, password)
    );
    if (!validateEmailAndPassword(email, password)) {
        return res.status(400).send('Invalid request body');
    }

    try {
        const hashedPassword = await argon2.hash(password);
        const result = await insertUser(email, hashedPassword, false);
        console.log('result', result);

        const userId = result?.rows[0].id;

        const token = createToken(email);

        console.log('token: ', token);

        sendSuccess(res, { token, userId });
    } catch (error) {
        if (error instanceof DatabaseError) {
            if (error.code === '23505') {
                return res
                    .status(409)
                    .send('User with given email already exists');
            }
        }
        if (error instanceof Error) {
            handleError(res, error);
        }
    }
});

router.post('/login', async (req, res) => {
    const { email, password } = req.body;

    if (!validateEmailAndPassword(email, password)) {
        return res.status(400).send('Invalid request body');
    }

    try {
        const result = await getUserByEmail(email);
        const user = result?.rows[0] as User;
        console.log('user', user);

        if (!user) {
            return res
                .status(404)
                .json({ error: `User with email: ${email} not found.` });
        }

        const isPasswordValid = await argon2.verify(
            user.password_hash,
            password
        );

        if (!isPasswordValid) {
            return res
                .status(401)
                .json({ error: 'Invalid username or password' });
        }

        const token = createToken(email);
        console.log('token: ', token);

        sendSuccess(res, { token, userId: user.id });
    } catch (error) {
        if (error instanceof Error) {
            handleError(res, error);
        }
    }
});

export default router;
