import express from 'express';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import { rateLimit } from 'express-rate-limit';

import usersRouter from './routers/usersRouter';
import muscleGroupsRouter from './routers/muscleGroupsRouter';
import exerciseRouter from './routers/exerciseRouter';
import trainingSetsRouter from './routers/trainingSetsRouter';
import { createTablesAndFillWithData } from './utils/utilFunctions';
import { authenticate, logger, unknownEndpoint } from './middleware/middleware';

const server = express();

const generalLimiter = rateLimit({
    windowMs: 1000 * 60 * 15,
    limit: 500,
});

server.use(
    cors({
        credentials: true,
    })
);
server.use(express.json());
server.use(cookieParser());

createTablesAndFillWithData();

server.use('/', generalLimiter);
server.use('/', logger);

server.use('/', usersRouter);
server.use('/', authenticate, muscleGroupsRouter);
server.use('/', authenticate, exerciseRouter);
server.use('/', authenticate, trainingSetsRouter);
server.use('/public', express.static(__dirname + '/public'));

server.use(unknownEndpoint);

export default server;
