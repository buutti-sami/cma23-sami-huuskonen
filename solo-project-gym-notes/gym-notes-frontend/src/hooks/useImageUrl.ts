import { useEffect, useState } from 'react';
import { getAIImage } from '../services/imageService';

export default function useImageUrl(token: string, resourceName: string) {
    const [imageUrl, setImageUrl] = useState<string>();

    useEffect(() => {
        let isUnMounted = false;

        if (token && !imageUrl) {
            if (!isUnMounted) {
                getAIImage(token, resourceName)
                    .then((url) => setImageUrl(url))
                    .catch((err) => console.log(err));
            }
        }

        return () => {
            isUnMounted = true;
        };
    }, [token, resourceName, imageUrl]);

    return { imageUrl };
}
