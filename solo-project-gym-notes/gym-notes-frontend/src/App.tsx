import {
    createBrowserRouter,
    Route,
    createRoutesFromElements,
    RouterProvider,
} from 'react-router-dom';
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';

import './App.css';
import SignUpPage from './pages/SignUpPage';
import LoginPage from './pages/LoginPage';
import AuthContextProvider from './contexts/authContext';
import HomePage from './pages/HomePage';
import ExercisesPerMuscleGroupPage from './pages/ExercisesPage';
import TrainingSetsPage from './pages/TrainingSetsPage';
import MuscleGroupsNameIdMappingProvider from './contexts/muscleGroupsNameIdMappingContext';
import ExercisesNameIdMappingProvider from './contexts/exercisesNameIdMappingContext';
import ErrorContextProvider from './contexts/errorContext';
import RootLayout from './layouts/RootLayout';
import MuscleGroupsPage from './pages/MuscleGroupsPage';
import NotFoundPage from './pages/NotFoundPage';

const router = createBrowserRouter(
    createRoutesFromElements(
        <Route path="/" element={<RootLayout />}>
            <Route index element={<HomePage />} />
            <Route path="/login" element={<LoginPage />} />
            <Route path="/signup" element={<SignUpPage />} />
            <Route path="/muscle-groups" element={<MuscleGroupsPage />} />
            <Route
                path="/muscle-groups/:muscleGroup"
                element={<ExercisesPerMuscleGroupPage />}
            />
            <Route
                path="/muscle-groups/:muscleGroup/:exercise"
                element={<TrainingSetsPage />}
            />

            <Route path="*" element={<NotFoundPage />} />
        </Route>
    )
);

function App() {
    return (
        <div className="App">
            <ErrorContextProvider>
                <AuthContextProvider>
                    <MuscleGroupsNameIdMappingProvider>
                        <ExercisesNameIdMappingProvider>
                            <RouterProvider router={router} />
                        </ExercisesNameIdMappingProvider>
                    </MuscleGroupsNameIdMappingProvider>
                </AuthContextProvider>
            </ErrorContextProvider>
        </div>
    );
}

export default App;
