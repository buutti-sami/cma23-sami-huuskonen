import { NavigateFunction } from 'react-router-dom';
import { SignUpLoginFormValues } from '../components/SignUpLoginForm';
import { login, signup } from '../services/userService';

export const handleLoginAndSignup = async (
    type: 'login' | 'signup',
    values: SignUpLoginFormValues,
    navigate: NavigateFunction
) => {
    try {
        const response = await (type === 'login'
            ? login(values.email, values.password)
            : signup(values.email, values.password));
        console.log('handleLoginAndSignup response: ', response);
        if (response?.ok) {
            const result = await response.json();
            navigate('/');
            return result;
        } else if (response?.status === 403) {
            throw new Error('User not found');
        } else if (response?.status === 401) {
            throw new Error('Wrong email or password');
        } else if (response?.status === 409) {
            throw new Error(`User with email ${values.email} already exists`);
        }
    } catch (error) {
        throw error;
    }
};
