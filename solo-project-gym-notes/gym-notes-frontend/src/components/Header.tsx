import { useAuthContext } from '../contexts/authContext';

import CustomButton from './CustomButton';
import styles from './Header.module.css';

export default function Header() {
    const { logOut, token } = useAuthContext();

    return (
        <div className={styles['header-container']}>
            <div className={styles['header-element-container']}></div>
            <div className={styles['header-element-container']}>
                <h1>Gym Notes</h1>
            </div>
            <div className={styles['header-element-container']}>
                {token && <CustomButton onClick={logOut}>Logout</CustomButton>}
            </div>
        </div>
    );
}
