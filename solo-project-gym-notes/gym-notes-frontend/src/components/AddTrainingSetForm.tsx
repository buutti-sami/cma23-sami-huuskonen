import { ErrorMessage, Field, Form, Formik } from 'formik';

import styles from './AddTrainingSetForm.module.css';
import CustomButton from './CustomButton';

export interface AddTrainingSetFormValues {
    weight: string;
    reps: string;
    description: string;
}
interface Props {
    handleFormSubmit: (values: AddTrainingSetFormValues) => void;
    initialValues?: AddTrainingSetFormValues | null;
}

export default function AddTrainingSetForm({
    handleFormSubmit,
    initialValues,
}: Props) {
    const validate = (values: AddTrainingSetFormValues) => {
        const errors: { weight?: string; reps?: string; description?: string } =
            {};

        if (!values.reps) {
            errors.reps = 'Add reps';
        } else if (isNaN(Number(values.reps))) {
            errors.reps = 'Use only whole numbers';
        } else if (!Number.isInteger(values.reps)) {
            errors.reps = 'Use only whole numbers';
        }

        if (!values.weight) {
            errors.weight = 'Add weight';
        } else if (isNaN(Number(values.weight))) {
            errors.weight =
                'Use only whole numbers or decimals like 20 or 45.5';
        }

        if (values.description) {
            if (values.description.length > 200) {
                errors.description = 'Description too long';
            }
        }

        return errors;
    };

    return (
        <div>
            <Formik
                initialValues={
                    initialValues || { weight: '', reps: '', description: '' }
                }
                validate={validate}
                onSubmit={(values, actions) => {
                    handleFormSubmit(values);
                    actions.setSubmitting(false);
                    actions.resetForm();
                }}
            >
                {({ isSubmitting }) => (
                    <Form>
                        <div className={styles['input-group']}>
                            <label>Weight</label>
                            <Field type="number" name="weight" />
                            <ErrorMessage
                                name="weight"
                                component="div"
                                className={styles['form-error']}
                            />
                        </div>
                        <div className={styles['input-group']}>
                            <label>Reps</label>
                            <Field type="number" name="reps" />
                            <ErrorMessage
                                name="reps"
                                component="div"
                                className={styles['form-error']}
                            />
                        </div>
                        <div className={styles['input-group']}>
                            <label>Description</label>
                            <Field
                                as="textarea"
                                name="description"
                                id={styles['description-input']}
                            />
                            <ErrorMessage
                                name="description"
                                component="div"
                                className={styles['form-error']}
                            />
                        </div>
                        <CustomButton type="submit" disabled={isSubmitting}>
                            Save
                        </CustomButton>
                    </Form>
                )}
            </Formik>
        </div>
    );
}
