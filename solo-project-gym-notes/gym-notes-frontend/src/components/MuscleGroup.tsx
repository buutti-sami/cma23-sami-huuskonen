import './MuscleGroup.module.css';
import { MuscleGroupType } from '../pages/MuscleGroupsPage';
import { Oval } from 'react-loader-spinner';
import { useAuthContext } from '../contexts/authContext';
import useImageUrl from '../hooks/useImageUrl';

interface Props {
    muscleGroup: MuscleGroupType;
}

export default function MuscleGroup({ muscleGroup }: Props) {
    const { token } = useAuthContext();
    const { imageUrl } = useImageUrl(
        token!,
        muscleGroup.name.replace(/ /g, '_')
    );

    const image = imageUrl ? (
        <img src={imageUrl} alt={muscleGroup.name} />
    ) : (
        <div
            style={{
                height: '50%',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
            }}
        >
            <Oval
                height={50}
                width={50}
                color="rgb(160, 255, 211)"
                wrapperStyle={{}}
                wrapperClass=""
                visible={true}
                ariaLabel="oval-loading"
                secondaryColor="gray"
                strokeWidth={2}
                strokeWidthSecondary={2}
            />
        </div>
    );

    return (
        <>
            {image}
            <p>
                <b>{muscleGroup.name}</b>
            </p>
        </>
    );
}
