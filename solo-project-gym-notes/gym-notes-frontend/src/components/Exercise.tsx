import { Oval } from 'react-loader-spinner';

import './Exercise.module.css';
import { ExerciseType } from '../pages/ExercisesPage';
import { useAuthContext } from '../contexts/authContext';
import useImageUrl from '../hooks/useImageUrl';

interface Props {
    exercise: ExerciseType;
}

export default function Exercise({ exercise }: Props) {
    const { token } = useAuthContext();
    const { imageUrl } = useImageUrl(token!, exercise.name.replace(/ /g, '_'));

    const image = imageUrl ? (
        <img src={imageUrl} alt={exercise.name} />
    ) : (
        <div
            style={{
                height: '50%',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
            }}
        >
            <Oval
                height={50}
                width={50}
                color="rgb(160, 255, 211)"
                wrapperStyle={{}}
                wrapperClass=""
                visible={true}
                ariaLabel="oval-loading"
                secondaryColor="gray"
                strokeWidth={2}
                strokeWidthSecondary={2}
            />
        </div>
    );

    return (
        <>
            {image}
            <p>
                <b>{exercise.name}</b>
            </p>
        </>
    );
}
