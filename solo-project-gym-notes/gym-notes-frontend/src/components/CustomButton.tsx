import { ReactNode } from 'react';

import './CustomButton.module.css';

interface Props {
    onClick?: () => void;
    children?: ReactNode;
    type?: 'button' | 'submit' | 'reset';
    disabled?: boolean;
}

export default function CustomButton({ type, children, ...props }: Props) {
    return (
        <button type={type} {...props}>
            {children}
        </button>
    );
}
