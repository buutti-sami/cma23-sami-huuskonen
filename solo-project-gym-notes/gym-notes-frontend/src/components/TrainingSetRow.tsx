import { AiOutlineDelete, AiOutlineEdit } from 'react-icons/ai';

import { TrainingSet } from '../pages/TrainingSetsPage';
import CustomButton from './CustomButton';

interface Props {
    set: TrainingSet;
    handleOpenEditModal: (set: TrainingSet) => void;
    handleDeleteSet: (set: TrainingSet) => void;
}

export default function TrainingSetRow({
    set,
    handleOpenEditModal,
    handleDeleteSet,
}: Props) {
    return (
        <tr>
            <td>{new Date(set.date).toLocaleDateString()}</td>
            <td>{set.weight}</td>
            <td>{set.reps}</td>
            <td>{set.description}</td>
            <td>
                <CustomButton onClick={() => handleOpenEditModal(set)}>
                    <AiOutlineEdit size={25} />
                </CustomButton>
            </td>
            <td>
                <CustomButton onClick={() => handleDeleteSet(set)}>
                    <AiOutlineDelete size={25} />
                </CustomButton>
            </td>
        </tr>
    );
}
