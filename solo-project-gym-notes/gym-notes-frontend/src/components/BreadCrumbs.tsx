import { Link } from 'react-router-dom';
import { useLocation } from 'react-router-dom';

import styles from './BreadCrumbs.module.css';

export default function BreadCrumbs() {
    const location = useLocation();

    const formatCrumb = (crumb: string) => {
        let formattedCrumb = '';
        formattedCrumb = `${crumb[0].toUpperCase()}${crumb.substring(1)}`;
        formattedCrumb = formattedCrumb.replace(/%20/g, ' ');
        return formattedCrumb;
    };

    let currentLink = '';

    const crumbs = location.pathname
        .split('/')
        .filter((crumb) => crumb !== '')
        .map((crumb) => {
            currentLink += `/${crumb}`;

            return (
                <div className={styles.crumb} key={crumb}>
                    <Link to={currentLink}>{formatCrumb(crumb)}</Link>
                </div>
            );
        });
    return <div className={styles.breadcrumbs}>{crumbs}</div>;
}
