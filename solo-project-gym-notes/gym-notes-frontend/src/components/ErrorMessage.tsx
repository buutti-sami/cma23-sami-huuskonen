import { useErrorContext } from '../contexts/errorContext';

import styles from './ErrorMessage.module.css';

export default function ErrorMessage() {
    const { error } = useErrorContext();

    return (
        <div
            className={styles['error-container']}
            style={{ display: error ? 'block' : 'none' }}
        >
            {error && error}
        </div>
    );
}
