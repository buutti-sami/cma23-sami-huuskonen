import { Formik, Form, Field, ErrorMessage } from 'formik';

import styles from './SignUpLoginForm.module.css';
import CustomButton from './CustomButton';

export interface SignUpLoginFormValues {
    email: string;
    password: string;
}

interface Props {
    handleFormSubmit: (values: SignUpLoginFormValues) => void;
}

export default function SignUpLoginForm({ handleFormSubmit }: Props) {
    const initialValues: SignUpLoginFormValues = { email: '', password: '' };

    const validate = (values: SignUpLoginFormValues) => {
        const errors: { email?: string; password?: string } = {};
        if (!values.email) {
            errors.email = 'Required';
        } else if (
            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
        ) {
            errors.email = 'Invalid email address';
        }

        if (!values.password) {
            errors.password = 'Required';
        }

        if (values.password.length < 8) {
            errors.password = 'Password min length is 8 characters';
        }

        return errors;
    };

    return (
        <div className={styles['form-container']}>
            <Formik
                initialValues={initialValues}
                validate={(values) => validate(values)}
                onSubmit={(values, actions) => {
                    handleFormSubmit(values);
                    actions.setSubmitting(false);
                }}
            >
                {({ isSubmitting }) => (
                    <Form>
                        <div className={styles['input-group']}>
                            <label htmlFor="email">Email</label>
                            <Field type="email" name="email" />
                            <ErrorMessage
                                name="email"
                                component="div"
                                className={styles['form-error']}
                            />
                        </div>
                        <div className={styles['input-group']}>
                            <label htmlFor="password">Password</label>
                            <Field type="password" name="password" />
                            <ErrorMessage
                                name="password"
                                component="div"
                                className={styles['form-error']}
                            />
                        </div>
                        <CustomButton type="submit" disabled={isSubmitting}>
                            Submit
                        </CustomButton>
                    </Form>
                )}
            </Formik>
        </div>
    );
}
