import { baseUrl } from './userService';

export interface TrainingSetWithoutId {
    userId: string;
    exerciseId: string;
    weight: number;
    reps: number;
    date: string;
    description: string | undefined;
}

export const addTrainingSet = async (
    trainingSet: TrainingSetWithoutId,
    token: string
) => {
    try {
        const response = await fetch(`${baseUrl}/training-sets`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(trainingSet),
        });
        const trainingSetId = await response.json();
        return trainingSetId;
    } catch (error) {
        // TODO add error message
        console.log('error: ', error);
    }
};

export const getTrainingSets = async (
    exerciseId: string,
    userId: string,
    token: string
) => {
    try {
        const response = await fetch(
            `${baseUrl}/training-sets/${exerciseId}?userId=${userId}`,
            {
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json',
                },
            }
        );

        const result = await response.json();
        return result;
    } catch (error) {
        // TODO send error message
        console.log(error);
    }
};

export const updateTrainingSet = async (
    token: string,
    trainingSetId: string,
    weight: string,
    reps: string,
    description: string
) => {
    try {
        const response = await fetch(
            `${baseUrl}/training-sets/${trainingSetId}`,
            {
                method: 'PATCH',
                headers: {
                    Authorization: `Bearer ${token}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ weight, reps, description }),
            }
        );

        const result = await response.json();
        return result;
    } catch (error) {
        console.log(error);
    }
};

export const deleteTrainingSet = async (
    token: string,
    trainingSetId: string
) => {
    try {
        await fetch(`${baseUrl}/training-sets/${trainingSetId}`, {
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });
    } catch (error) {
        console.log(error);
    }
};
