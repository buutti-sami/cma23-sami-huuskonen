import { baseUrl } from './userService';

export const getAIImage = async (token: string, resourceName: string) => {
    try {
        const response = await fetch(
            `${baseUrl}/public/images/${resourceName.toLowerCase()}.png`,
            {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            }
        );

        const imageBlob = await response.blob();
        const imageUrl = URL.createObjectURL(imageBlob);
        return imageUrl;
    } catch (error) {
        console.log(error);
    }
};
