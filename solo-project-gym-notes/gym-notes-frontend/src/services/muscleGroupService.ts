import { baseUrl } from './userService';

export const getAllMuscleGroups = async (token: string) => {
    try {
        const response = await fetch(`${baseUrl}/muscle-groups`, {
            headers: {
                Authorization: 'Bearer ' + token,
            },
        });
        const result = await response.json();
        return result;
    } catch (error) {
        // TODO set error message
        console.log(error);
    }
};
