export const baseUrl = 'http://localhost:3003';

export const signup = async (email: string, password: string) => {
    try {
        const response = await fetch(`${baseUrl}/signup`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ email, password }),
        });
        return response;
    } catch (error) {
        // TODO set error message
        console.log(error);
    }
};

export const login = async (email: string, password: string) => {
    try {
        const response = await fetch(`${baseUrl}/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ email, password }),
        });

        return response;
    } catch (error) {
        // TODO set error message
        console.log(error);
    }
};
