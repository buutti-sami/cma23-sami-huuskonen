import { baseUrl } from './userService';

export const getAllExercises = async (token: string) => {
    try {
        const response = await fetch(`${baseUrl}/exercises`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });
        const exercises = await response.json();
        return exercises;
    } catch (error) {
        // TODO set error message
        console.log(error);
    }
};

export const getExercisesByMuscleGroupId = async (
    muscleGroupId: string,
    token: string
) => {
    try {
        const response = await fetch(
            `${baseUrl}/exercises?muscleGroupId=${muscleGroupId}`,
            {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            }
        );
        const exercises = await response.json();
        return exercises;
    } catch (error) {
        // TODO set error message
        console.log(error);
    }
};

export const getExerciseById = async (id: string, token: string) => {
    try {
        const response = await fetch(`${baseUrl}/${id}`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });
        const exercise = await response.json();
        return exercise;
    } catch (error) {
        // TODO set error message
        console.log(error);
    }
};
