import { Link } from 'react-router-dom';

export default function NotFoundPage() {
    return (
        <div>
            <h3>Sorry, page not found</h3>
            <Link to="/">Go to Home</Link>
        </div>
    );
}
