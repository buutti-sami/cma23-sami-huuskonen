import { useEffect, useState } from 'react';
import { Link, Navigate, useParams } from 'react-router-dom';

import styles from './ExercisesPage.module.css';
import { getExercisesByMuscleGroupId } from '../services/exercisesService';
import { useAuthContext } from '../contexts/authContext';
import { useMuscleGroupsNameIdMappingContext } from '../contexts/muscleGroupsNameIdMappingContext';
import { useExercisesNameIdMappingContext } from '../contexts/exercisesNameIdMappingContext';
import Exercise from '../components/Exercise';

export interface ExerciseType {
    id: string;
    name: string;
    muscleGroupId: string;
    description: string;
}

export default function ExercisesPerMuscleGroupPage() {
    const [exercises, setExercises] = useState<ExerciseType[]>([]);
    const { muscleGroup } = useParams();
    const { muscleGroupsNameIdMapping } = useMuscleGroupsNameIdMappingContext();
    const { setExercisesNameIdMapping } = useExercisesNameIdMappingContext();
    const { token } = useAuthContext();

    const muscleGroupId = muscleGroupsNameIdMapping[muscleGroup!];

    useEffect(() => {
        if (token && exercises.length < 1) {
            getExercisesByMuscleGroupId(String(muscleGroupId), token)
                .then((data) => {
                    setExercises(data.exercises);
                    setExercisesNameIdMapping(data.exercisesNameIdMapping);
                })
                .catch((err) => console.log(err));
        }
    }, [muscleGroupId, token, setExercisesNameIdMapping, exercises]);

    if (!token) {
        return <Navigate to="/" />;
    }

    return (
        <>
            <h2>{`EXERCISES FOR ${muscleGroup?.toUpperCase()}`}</h2>
            <div className={styles['selection-grid']}>
                {exercises.map((exercise) => (
                    <div key={exercise.id}>
                        <Link
                            to={`/muscle-groups/${muscleGroup}/${exercise.name.toLowerCase()}`}
                        >
                            <div
                                className={
                                    styles['selection-element-container']
                                }
                            >
                                <Exercise exercise={exercise} />
                            </div>
                        </Link>
                    </div>
                ))}
            </div>
        </>
    );
}
