import { useEffect, useState } from 'react';
import { Navigate, useParams } from 'react-router-dom';
import { Box, Modal } from '@mui/material';

import { useAuthContext } from '../contexts/authContext';
import styles from './TrainingSetsPage.module.css';
import {
    TrainingSetWithoutId,
    addTrainingSet,
    deleteTrainingSet,
    getTrainingSets,
    updateTrainingSet,
} from '../services/trainingSetsService';
import { useExercisesNameIdMappingContext } from '../contexts/exercisesNameIdMappingContext';
import AddTrainingSetForm, {
    AddTrainingSetFormValues,
} from '../components/AddTrainingSetForm';
import TrainingSetRow from '../components/TrainingSetRow';
import CustomButton from '../components/CustomButton';

export interface TrainingSet {
    id: string;
    userId: string;
    exerciseId: string;
    weight: number;
    reps: number;
    date: string;
    description: string | undefined;
}

const modalContainerStyle = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%',
};

const formContainerStyle = {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: '10px',
    border: '1px solid black',
    height: '30%',
};

export default function TrainingSetsPage() {
    const [allTrainingSets, setAllTrainingSets] = useState<
        TrainingSet[] | null
    >(null);
    const [isEditModalOpen, setIsEditModalOpen] = useState<boolean>(false);
    const [formInitialValues, setFormInitialValues] =
        useState<AddTrainingSetFormValues | null>(null);
    const [currentTrainingSet, setCurrentTrainingSet] =
        useState<TrainingSet | null>(null);

    const { exercise } = useParams();
    const { token, userId } = useAuthContext();
    const { exercisesNameIdMapping } = useExercisesNameIdMappingContext();

    const exerciseId = exercisesNameIdMapping[exercise!];

    const historicTrainingSets = allTrainingSets
        ?.filter((set) => set.date !== new Date().toISOString().slice(0, 10))
        .sort((a, b) => {
            const dateA = new Date(a.date) as any;
            const dateB = new Date(b.date) as any;
            return dateB - dateA;
        });

    const currentTrainingSets = allTrainingSets?.filter(
        (set) => set.date === new Date().toISOString().slice(0, 10)
    );

    useEffect(() => {
        if (token && userId) {
            getTrainingSets(String(exerciseId), String(userId), token).then(
                (data) => {
                    setAllTrainingSets(data);
                }
            );
        }
    }, [token, userId, exerciseId]);

    useEffect(() => {
        setFormInitialValues(null);
    }, []);

    useEffect(() => {
        if (formInitialValues) {
            setIsEditModalOpen(true);
        }
    }, [formInitialValues]);

    const handleSubmitForm = (values: AddTrainingSetFormValues) => {
        const formattedDate = new Date().toISOString().slice(0, 10);

        const trainingSet: TrainingSetWithoutId = {
            weight: Number(values.weight),
            reps: Number(values.reps),
            date: formattedDate,
            userId: userId!,
            exerciseId: exerciseId!,
            description: values.description,
        };

        addTrainingSet(trainingSet, token!)
            .then((trainingSetId) => {
                setAllTrainingSets([
                    ...(allTrainingSets || []),
                    { ...trainingSet, id: trainingSetId },
                ]);
            })
            .catch((err) => console.log(err));
    };

    const handleUpdateTrainingSet = async (
        values: AddTrainingSetFormValues
    ) => {
        try {
            if (currentTrainingSet) {
                await updateTrainingSet(
                    token!,
                    currentTrainingSet.id,
                    values.weight,
                    values.reps,
                    values.description || ' '
                );

                const updatedTrainingSets = allTrainingSets?.map((set) =>
                    set.id === currentTrainingSet.id
                        ? {
                              id: currentTrainingSet.id,
                              weight: Number(values.weight),
                              reps: Number(values.reps),
                              description: values.description,
                              userId: userId!,
                              exerciseId: currentTrainingSet.exerciseId,
                              date: currentTrainingSet.date,
                          }
                        : set
                );

                setAllTrainingSets(updatedTrainingSets || allTrainingSets);
                setIsEditModalOpen(false);
                setFormInitialValues(null);
            }
        } catch (error) {
            console.log(error);
        }
    };

    const handleOpenEditModal = (set: TrainingSet) => {
        setFormInitialValues({
            reps: String(set.reps),
            weight: String(set.weight),
            description: set.description || '',
        });
        setCurrentTrainingSet(set);
        setIsEditModalOpen(true);
    };

    const handleDeleteSet = async (setToDelete: TrainingSet) => {
        try {
            await deleteTrainingSet(token!, setToDelete.id);

            if (allTrainingSets) {
                const updatedTrainingSets = allTrainingSets?.filter(
                    (set) => set.id !== setToDelete.id
                );
                setAllTrainingSets(updatedTrainingSets);
            }
        } catch (error) {
            console.log(error);
        }
    };

    if (!token) {
        return <Navigate to="/" />;
    }

    const trainingSetsTable = (trainingSets: TrainingSet[] | undefined) => {
        return (
            <table>
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Weight</th>
                        <th>Reps</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    {trainingSets?.map((set) => (
                        <TrainingSetRow
                            key={set.id}
                            set={set}
                            handleOpenEditModal={handleOpenEditModal}
                            handleDeleteSet={handleDeleteSet}
                        />
                    ))}
                </tbody>
            </table>
        );
    };

    return (
        <div className={styles['training-sets-page-container']}>
            <Modal open={isEditModalOpen}>
                <Box sx={modalContainerStyle}>
                    <Box sx={formContainerStyle}>
                        <div className={styles.modalHeaderContainer}>
                            <div className={styles.modalHeaderElement} />
                            <div className={styles.modalHeaderElement}>
                                <h3>Update set</h3>
                            </div>
                            <div className={styles.modalHeaderElement}>
                                <CustomButton
                                    onClick={() => setIsEditModalOpen(false)}
                                >
                                    X
                                </CustomButton>
                            </div>
                        </div>
                        <AddTrainingSetForm
                            handleFormSubmit={handleUpdateTrainingSet}
                            initialValues={formInitialValues}
                        />
                    </Box>
                </Box>
            </Modal>
            <h2>{exercise?.toUpperCase()}</h2>
            <div className={styles['training-sets-current-section']}>
                <div className={styles['training-sets-form']}>
                    <AddTrainingSetForm handleFormSubmit={handleSubmitForm} />
                </div>
                <div className={styles['training-sets-current-sets']}>
                    {trainingSetsTable(currentTrainingSets)}
                </div>
            </div>
            <div className={styles['training-sets-history-section']}>
                {trainingSetsTable(historicTrainingSets)}
            </div>
        </div>
    );
}
