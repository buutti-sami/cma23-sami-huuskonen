import { useEffect } from 'react';
import { useAuthContext } from '../contexts/authContext';
import MuscleGroupsPage from './MuscleGroupsPage';
import SignUpPage from './SignUpPage';

export default function HomePage() {
    const {
        token,
        userId,
        setTokenToContextAndStorage,
        setUserIdToContextAndStorage,
    } = useAuthContext();

    useEffect(() => {
        if (!token) {
            const tokenFromLocalStorage = localStorage.getItem('token');
            if (tokenFromLocalStorage) {
                setTokenToContextAndStorage(tokenFromLocalStorage);
            }
        }

        if (!userId) {
            const userIdFromLocalStorage = localStorage.getItem('userId');
            if (userIdFromLocalStorage) {
                setUserIdToContextAndStorage(userIdFromLocalStorage);
            }
        }
    }, [
        token,
        setTokenToContextAndStorage,
        userId,
        setUserIdToContextAndStorage,
    ]);

    return <>{token ? <MuscleGroupsPage /> : <SignUpPage />}</>;
}
