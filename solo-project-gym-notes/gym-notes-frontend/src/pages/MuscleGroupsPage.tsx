import { useEffect, useState } from 'react';

import styles from './MuscleGroupsPage.module.css';
import { getAllMuscleGroups } from '../services/muscleGroupService';
import { useAuthContext } from '../contexts/authContext';
import { Link, Navigate } from 'react-router-dom';
import { useMuscleGroupsNameIdMappingContext } from '../contexts/muscleGroupsNameIdMappingContext';
import MuscleGroup from '../components/MuscleGroup';

export interface MuscleGroupType {
    name: string;
    description: string;
    id: number | string;
}

export default function MuscleGroupsPage() {
    const [muscleGroups, setMuscleGroups] = useState<MuscleGroupType[]>([]);

    const { token } = useAuthContext();
    const { setMuscleGroupsNameIdMapping } =
        useMuscleGroupsNameIdMappingContext();

    useEffect(() => {
        if (token && muscleGroups.length < 1) {
            getAllMuscleGroups(token)
                .then((result) => {
                    setMuscleGroups(result.muscleGroups);
                    setMuscleGroupsNameIdMapping(
                        result.muscleGroupsNameIdMapping
                    );
                })
                .catch((err) => {
                    console.log(err);
                });
        }
    }, [token, setMuscleGroupsNameIdMapping, muscleGroups]);

    if (!token) {
        return <Navigate to="/" />;
    }

    return (
        <>
            <h3>MUSCLE GROUPS</h3>
            <div className={styles['selection-grid']}>
                {muscleGroups.map((muscleGroup) => (
                    <div key={muscleGroup.id}>
                        <Link
                            to={`/muscle-groups/${muscleGroup.name.toLowerCase()}`}
                        >
                            <div
                                className={
                                    styles['selection-element-container']
                                }
                            >
                                <MuscleGroup muscleGroup={muscleGroup} />
                            </div>
                        </Link>
                    </div>
                ))}
            </div>
        </>
    );
}
