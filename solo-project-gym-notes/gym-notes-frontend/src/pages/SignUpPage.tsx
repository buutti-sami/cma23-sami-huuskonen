import styles from './SignUpLoginPage.module.css';
import SignUpLoginForm, {
    SignUpLoginFormValues,
} from '../components/SignUpLoginForm';
import { Link, useNavigate } from 'react-router-dom';
import { handleLoginAndSignup } from '../utils/formUtils';
import { useAuthContext } from '../contexts/authContext';
import { useErrorContext } from '../contexts/errorContext';

export default function SignUp() {
    const navigate = useNavigate();
    const { setTokenToContextAndStorage, setUserIdToContextAndStorage } =
        useAuthContext();
    const { handleSetError } = useErrorContext();

    const handleFormSubmit = async (values: SignUpLoginFormValues) => {
        try {
            const { token, userId } = await handleLoginAndSignup(
                'signup',
                values,
                navigate
            );
            if (token && userId) {
                setTokenToContextAndStorage(token);
                setUserIdToContextAndStorage(userId);
            }
        } catch (error) {
            if (error instanceof Error) {
                handleSetError(error.message);
            }
        }
    };

    return (
        <div className={styles['signup-login-page']}>
            <h2>Sign Up</h2>
            <SignUpLoginForm handleFormSubmit={handleFormSubmit} />
            <Link to="/login">Already have an account? Login here.</Link>
        </div>
    );
}
