import { ReactNode, createContext, useContext, useState } from 'react';

interface MuscleGroupsNameIdMapping {
    [key: string]: string;
}
interface MuscleGroupsNameIdMappingContextType {
    muscleGroupsNameIdMapping: MuscleGroupsNameIdMapping;
    setMuscleGroupsNameIdMapping: (value: MuscleGroupsNameIdMapping) => void;
}

interface Props {
    children: ReactNode;
}

export const MuscleGroupsNameIdMappingContext =
    createContext<MuscleGroupsNameIdMappingContextType | null>(null);

export default function MuscleGroupsNameIdMappingProvider({ children }: Props) {
    const [muscleGroupsNameIdMapping, setMuscleGroupsNameIdMapping] =
        useState<MuscleGroupsNameIdMapping>({});

    return (
        <MuscleGroupsNameIdMappingContext.Provider
            value={{
                muscleGroupsNameIdMapping,
                setMuscleGroupsNameIdMapping,
            }}
        >
            {children}
        </MuscleGroupsNameIdMappingContext.Provider>
    );
}

export function useMuscleGroupsNameIdMappingContext() {
    const context = useContext(MuscleGroupsNameIdMappingContext);
    if (!context) {
        throw new Error(
            'useMuscleGroupsNameIdMappingContext must be used within MuscleGroupsNameIdMappingContext   '
        );
    }
    return context;
}
