import { ReactNode, createContext, useContext, useState } from 'react';

interface ErrorContextType {
    error: string | null;
    handleSetError: (message: string) => void;
}

export const ErrorContext = createContext<ErrorContextType | null>(null);

interface Props {
    children: ReactNode;
}

export default function ErrorContextProvider({ children }: Props) {
    const [error, setError] = useState<null | string>(null);

    const handleSetError = (message: string) => {
        setError(message);
        setTimeout(() => {
            setError(null);
        }, 5000);
    };

    return (
        <ErrorContext.Provider value={{ error, handleSetError }}>
            {children}
        </ErrorContext.Provider>
    );
}

export const useErrorContext = () => {
    const context = useContext(ErrorContext);
    if (!context) {
        throw new Error(
            'useErrorContext must be used within ErrorContextProvider'
        );
    }
    return context;
};
