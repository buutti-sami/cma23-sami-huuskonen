import { ReactNode, createContext, useContext, useState } from 'react';

interface ExercisesNameIdMapping {
    [key: string]: string;
}

interface ExercisesNameIdMappingContextType {
    exercisesNameIdMapping: ExercisesNameIdMapping;
    setExercisesNameIdMapping: (value: ExercisesNameIdMapping) => void;
}

export const ExercisesNameIdMappingContext =
    createContext<ExercisesNameIdMappingContextType | null>(null);

export default function ExercisesNameIdMappingProvider({
    children,
}: {
    children: ReactNode;
}) {
    const [exercisesNameIdMapping, setExercisesNameIdMapping] =
        useState<ExercisesNameIdMapping>({});

    return (
        <ExercisesNameIdMappingContext.Provider
            value={{ exercisesNameIdMapping, setExercisesNameIdMapping }}
        >
            {children}
        </ExercisesNameIdMappingContext.Provider>
    );
}

export function useExercisesNameIdMappingContext() {
    const context = useContext(ExercisesNameIdMappingContext);
    if (!context) {
        throw new Error(
            'useExercisesNameIdMappingContext must be used within ExercisesNameIdMappingProvider'
        );
    }
    return context;
}
