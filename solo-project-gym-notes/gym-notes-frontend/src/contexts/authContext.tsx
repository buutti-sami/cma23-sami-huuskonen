import { ReactNode, createContext, useContext, useState } from 'react';

interface Props {
    children: ReactNode;
}

interface AuthContextType {
    token: string | null;
    userId: string | null;
    setTokenToContextAndStorage: (value: string) => void;
    setUserIdToContextAndStorage: (value: string) => void;
    logOut: () => void;
}

export const AuthContext = createContext<AuthContextType | null>(null);

export default function AuthContextProvider({ children }: Props) {
    const [token, setToken] = useState<string | null>(null);
    const [userId, setUserId] = useState<string | null>(null);

    const setTokenToContextAndStorage = (token: string) => {
        setToken(token);
        localStorage.setItem('token', token);
    };

    const setUserIdToContextAndStorage = (userId: string) => {
        setUserId(userId);
        localStorage.setItem('userId', userId);
    };

    const logOut = () => {
        setUserId(null);
        setToken(null);
        localStorage.clear();
    };

    return (
        <AuthContext.Provider
            value={{
                token,
                userId,
                setTokenToContextAndStorage,
                setUserIdToContextAndStorage,
                logOut,
            }}
        >
            {children}
        </AuthContext.Provider>
    );
}

export function useAuthContext() {
    const context = useContext(AuthContext);
    if (!context) {
        throw new Error(
            'useAuthContext must be used within a AuthContextProvider'
        );
    }
    return context;
}
