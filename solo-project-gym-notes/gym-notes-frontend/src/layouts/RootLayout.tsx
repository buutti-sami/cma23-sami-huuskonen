import { Outlet } from 'react-router-dom';
import Header from '../components/Header';
import BreadCrumbs from '../components/BreadCrumbs';
import ErrorMessage from '../components/ErrorMessage';

export default function RootLayout() {
    return (
        <>
            <Header />
            <BreadCrumbs />
            <ErrorMessage />
            <Outlet />
        </>
    );
}
