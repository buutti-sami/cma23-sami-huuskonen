# Gym Notes Repository

This repository contains the source code for a web application with separate frontend and backend directories.

## Frontend

The frontend of this web app is built using React and TypeScript. To set up and run the frontend, follow these steps:

1. Navigate to the `frontend` directory:
   cd gym-notes-frontend

2. Install the required dependencies:
   npm install

3. Start the development server:
   npm start

The frontend should now be running at [http://localhost:3000](http://localhost:3000).

## Backend

The backend of this web app is built using Node.js, TypeScript, and PostgreSQL. You will need to set up environment variables for database configuration and an access key for the OpenAI API.

### Database Configuration

Create a `.env` file in the `backend` directory with the following environment variables:

PORT=<port_for_the_app_to_run_in>
JWT_SECRET=<JWT_secret_for_token_authentication>
PG_USER=<your_postgres_user>
PG_PASSWORD=<your_postgres_password>
PG_DATABASE=<your_postgres_database>
PG_HOST=<your_postgres_host>
PG_PORT=<your_postgres_port>
OPENAI_KEY=<your_openai_api_key>

Make sure to replace `<your_postgres_user>`, `<your_postgres_password>`, `<your_postgres_database>`, `<your_postgres_host>`, `<your_postgres_port>`, and `<your_openai_api_key>` with your actual database and API credentials.

To set up the backend, follow these steps:

1. Navigate to the `backend` directory:
   cd backend

2. Install the required dependencies:
   npm install

3. If you don't have ts-node installed globally. Install it with:
   npm install ts-node

4. Start the development server with automatic TypeScript compilation:
   npm run dev

## Deployment

Before deploying this web app to a production environment, make sure to update the environment variables in the `.env` file and follow best practices for securing your credentials.

## Additional Information

For more information on the individual components, please refer to the `frontend/README.md` and `backend/README.md` files for each respective part of the application.
