import express from 'express';
import argon2 from 'argon2';
import dotenv from 'dotenv';
import jwt from 'jsonwebtoken';

const router = express.Router();

dotenv.config();

const JWT_SECRET = process.env.JWT_SECRET as string;
interface User {
    username: string;
    password: string;
}

const users: User[] = [];

const createToken = (username: string) => {
    return jwt.sign({ username }, JWT_SECRET, {
        expiresIn: 60000 * 15,
    });
};

router.post('/register', async (req, res) => {
    const { username, password } = req.body;

    if (!username || !password) {
        return res
            .status(400)
            .send({ error: 'username or password not provided' });
    }

    const existingUser = users.find((user) => user.username === username);
    if (existingUser) {
        return res.status(409).send({ message: 'User already exists' });
    }

    try {
        const hashedPassword = await argon2.hash(password);
        console.log(hashedPassword);
        users.push({ username, password: hashedPassword });

        const token = createToken(username);
        res.status(200).json(token);
    } catch (error) {
        console.log(error);
    }
});

router.post('/login', async (req, res) => {
    const { username, password } = req.body;

    if (!username || !password) {
        return res
            .status(400)
            .send({ error: 'username or password not provided' });
    }

    const user = users.find((user) => user.username === username);

    if (!user) {
        return res.status(401).json({ message: 'User not found' });
    }

    const isPasswordValid = await argon2.verify(user.password, password);

    if (!isPasswordValid) {
        return res
            .status(401)
            .json({ message: 'Invalid username or password' });
    }

    if (isPasswordValid) {
        const token = createToken(username);
        return res.status(200).json(token);
    }
});

router.post('/admin', async (req, res) => {
    const { username, password } = req.body;

    if (!username || !password) {
        return res
            .status(400)
            .send({ message: 'username or password not provided' });
    }

    const adminUsername = process.env.ADMIN_USERNAME!;
    const adminPasswordHash = process.env.ADMIN_PASSWORD_HASH!;

    const isPasswordValid = await argon2.verify(adminPasswordHash, password);

    if (
        adminUsername.toLowerCase() === username.toLowerCase() &&
        isPasswordValid
    ) {
        return res.status(204).send();
    } else {
        return res.status(401).send();
    }
});
export default router;
