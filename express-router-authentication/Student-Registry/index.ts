import express from 'express';
import { logRequest, unknownEndpoint } from './middleware/middleware';
import studentsRouter from './routers/studentsRouter';
import usersRouter from './routers/usersRouter';
import 'dotenv/config';

const server = express();
server.use(express.json());

const PORT = process.env.PORT;

server.use(logRequest);
server.use(express.static('public'));

server.get('/', (req, res) => {
    res.send('OK');
});

server.use('/', studentsRouter);
server.use('/', usersRouter);

server.use(unknownEndpoint);

server.listen(PORT, () => {
    console.log(`listening on port ${PORT}`);
});
