import jwt from 'jsonwebtoken';
import readline from 'readline-sync';

const token = readline.question('Token: ');
const secret = readline.question('Secret: ');

try {
    const verifiedToken = jwt.verify(token, secret);
    console.log(verifiedToken);
} catch (error) {
    console.log('error verifying the token');
}
