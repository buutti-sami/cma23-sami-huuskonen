import express from 'express';
import {
    authenticate,
    validatePostRequest,
    validatePutRequest,
} from '../middleware/middleware';

const router = express.Router();

interface Student {
    id: string;
    name: string;
    email: string;
}

let students: Student[] = [];

router.get('/students', authenticate, (req, res) => {
    res.status(200).json(students.map((student) => student.id));
});

router.get('/student/:id', authenticate, (req, res) => {
    const { id } = req.params;
    const student = students.find((student) => student.id === id);

    if (student) {
        res.json(student);
    } else {
        res.status(404).send(`Student with ${id} not found.`);
    }
});

router.post('/student', validatePostRequest, authenticate, (req, res) => {
    const { id, name, email } = req.body;
    students.push({ id, name, email });
    res.status(201).send();
});

router.put('/student/:id', validatePutRequest, authenticate, (req, res) => {
    const { id } = req.params;
    const { name, email } = req.body;

    const studentToUpdate = students.find((student) => student.id === id);
    if (!studentToUpdate) {
        return res.status(404).json({ error: 'Student not found' });
    }

    const updatedStudent: Student = {
        ...studentToUpdate,
        name: name || studentToUpdate.name,
        email: email || studentToUpdate.email,
    };

    students = students.map((student) =>
        student.id === updatedStudent.id ? updatedStudent : student
    );

    return res.status(204).send();
});

router.delete('/student/:id', authenticate, (req, res) => {
    const { id } = req.params;

    const studentToDelete = students.find((student) => student.id === id);

    if (!studentToDelete) {
        return res.status(404).send({ error: 'no student found' });
    }

    students = students.filter((student) => student.id !== id);
    return res.status(204).send();
});

export default router;
