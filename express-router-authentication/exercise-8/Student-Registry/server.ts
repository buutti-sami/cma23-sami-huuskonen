import express from 'express';
import { logRequest, unknownEndpoint } from './middleware/middleware';
import studentsRouter from './routers/studentsRouter';
import usersRouter from './routers/usersRouter';

const server = express();
server.use(express.json());

server.use(logRequest);
server.use(express.static('public'));

server.get('/', (req, res) => {
    res.send('OK');
});

server.use('/', studentsRouter);
server.use('/', usersRouter);

server.use(unknownEndpoint);

export default server;
