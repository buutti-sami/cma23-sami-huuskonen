import request from 'supertest';
import server from '../server';

describe('/user', () => {
    describe('/register', () => {
        it('registers a new user and logs in if valid password and username are provided', (done) => {
            const user = {
                username: 'testuser',
                password: 'secret',
            };

            request(server)
                .post('/register')
                .send(user)
                .expect(200)
                .end((err) => {
                    if (err) {
                        return done(err);
                    }

                    request(server)
                        .post('/login')
                        .send(user)
                        .expect(200)
                        .end((err2) => {
                            if (err2) {
                                return done(err2);
                            }

                            done();
                        });
                });
        });

        it('registers a new user but does not log in if invalid password and username are provided', (done) => {
            const user = {
                username: 'testuser2',
                password: 'secret',
            };

            request(server)
                .post('/register')
                .send(user)
                .expect(200)
                .end((err) => {
                    if (err) {
                        return done(err);
                    }

                    request(server)
                        .post('/login')
                        .send({ ...user, password: 'wrongpass' })
                        .expect(401)
                        .end((err2) => {
                            if (err2) {
                                return done(err2);
                            }

                            done();
                        });
                });
        });

        it('does not register a new user if password is missing', () => {
            const user = {
                username: 'testuser',
            };

            request(server).post('/register').send(user).expect(400);
        });

        it('does not register a new user if username is missing', () => {
            const user = {
                password: 'testpass',
            };

            request(server).post('/register').send(user).expect(400);
        });
    });
});
