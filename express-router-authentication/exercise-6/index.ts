import jwt from 'jsonwebtoken';
import readline from 'readline-sync';

const username = readline.question('username: ');
const password = readline.question('password: ');

const token = jwt.sign({ username }, password, {
    expiresIn: '15 minutes',
});

console.log(token);
