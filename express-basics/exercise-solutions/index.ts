// Exercise 1***************************
// import http from 'http';

// const server = http.createServer((req, res) => {
//     res.write('Hello World!');
//     res.end();
// });

// const PORT = 3003;

// server.listen(PORT);
// console.log('server listening on port: ', PORT);

// Exercise 2 *********************************
// import express from 'express';

// const app = express();

// app.get('/', (req, res) => {
//     res.send('Hello World!');
// });

// app.get('/movies', (req, res) => {
//     res.send(JSON.stringify(['Pulp Fiction', 'Dumb and Dumber'], null, 2));
// });

// const PORT = 3003;

// app.listen(PORT, () => {
//     console.log('listening on port: ', PORT);
// });

// Exercise 3 **********************************
// import express from 'express';

// const server = express();

// let count = 0;

// server.get('/counter/:name', (req, res) => {
//     const { number } = req.query;

//     if (typeof number === 'string') {
//         count = parseInt(number);
//     }

//     res.json(count);
//     count++;
// });

// const PORT = 3003;

// server.listen(PORT, () => {
//     console.log('listening on port', PORT);
// });

// Exercise 4 ********************************************************
// import express from 'express';

// const server = express();

// let count = 0;

// interface VisitorCounts {
//     [visitor: string]: number;
// }

// const visitorCounts: VisitorCounts = {};

// server.get('/counter/:name', (req, res) => {
//     const { number } = req.query;
//     const { name } = req.params;

//     if (typeof number === 'string') {
//         count = parseInt(number);
//     }

//     if (typeof name === 'string') {
//         if (name in visitorCounts) {
//             visitorCounts[name]++;
//             count++;
//             res.json(`${name} was here ${visitorCounts[name]} times`);
//             return;
//         } else {
//             visitorCounts[name] = 0;
//             count++;
//             res.json(`${name}'s first time here.`);
//             return;
//         }
//     }

//     res.json(count);
//     count++;
// });

// const PORT = 3003;

// server.listen(PORT, () => {
//     console.log('listening on port', PORT);
// });
