import express from 'express';
import {
    logRequest,
    unknownEndpoint,
    validatePostRequest,
    validatePutRequest,
} from './middleware/middleware';

const server = express();
server.use(express.json());

const PORT = 3003;

server.use(logRequest);

interface Student {
    id: string;
    name: string;
    email: string;
}

let students: Student[] = [];

server.get('/students', (req, res) => {
    res.status(200).json(students.map((student) => student.id));
});

server.get('/student/:id', (req, res) => {
    const { id } = req.params;
    const student = students.find((student) => student.id === id);

    if (student) {
        res.json(student);
    } else {
        res.status(404).send(`Student with ${id} not found.`);
    }
});

server.post('/student', validatePostRequest, (req, res) => {
    const { id, name, email } = req.body;
    students.push({ id, name, email });
    res.status(201).send();
});

server.put('/student/:id', validatePutRequest, (req, res) => {
    const { id } = req.params;
    const { name, email } = req.body;

    const studentToUpdate = students.find((student) => student.id === id);
    if (!studentToUpdate) {
        return res.status(404).json({ error: 'Student not found' });
    }

    const updatedStudent: Student = {
        ...studentToUpdate,
        name: name || studentToUpdate.name,
        email: email || studentToUpdate.email,
    };

    students = students.map((student) =>
        student.id === updatedStudent.id ? updatedStudent : student
    );

    return res.status(204).send();
});

server.delete('/student/:id', (req, res) => {
    const { id } = req.params;

    const studentToDelete = students.find((student) => student.id === id);

    if (!studentToDelete) {
        return res.status(404).send({ error: 'no student found' });
    }

    students = students.filter((student) => student.id !== id);
    return res.status(204).send();
});

server.use(unknownEndpoint);

server.listen(PORT, () => {
    console.log(`listening on port ${PORT}`);
});
