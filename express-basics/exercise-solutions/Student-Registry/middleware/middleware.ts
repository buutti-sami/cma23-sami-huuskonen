import { NextFunction, Request, Response } from 'express';

export const logRequest = (
    req: Request,
    _res: Response,
    next: NextFunction
) => {
    const now = new Date().toLocaleString();

    console.log('Time of request: ', now);

    console.log('Method of request: ', req.method);

    console.log('URL of request: ', req.originalUrl);

    if (req.body) {
        console.log('Request body: ', req.body);
    }

    next();
};

export const unknownEndpoint = (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    res.status(404).send({ error: 'You should not be here' });
    next();
};

export const validatePostRequest = (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    const { id, name, email } = req.body;

    if (!id || !name || !email) {
        res.status(400).send({ error: 'Request body is missing parameters' });
        next();
    }
    next();
};

export const validatePutRequest = (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    const { name, email } = req.body;

    if (!email && !name) {
        return res
            .status(400)
            .send({ error: 'Request body is missing parameters' });
    }
    next();
};
