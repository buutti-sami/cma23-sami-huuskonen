import React, { useEffect, useState } from 'react';
import './App.css';
import TodoNote from './components/TodoNote';
import InputForm from './components/InputForm';
import Search from './components/Search';
import {
    addNote,
    deleteNote,
    getNotes,
    updateNote,
} from './services/notesService';

export interface Todo {
    id: number;
    text: string;
    complete: boolean;
}

export const randomId = () => Math.random() * 100000;

function App() {
    const [todos, setTodos] = useState<Todo[]>([]);
    const [searchQuery, setSearchQuery] = useState<string>('');

    useEffect(() => {
        try {
            getNotes()?.then((data) => setTodos(data));
        } catch (error) {
            console.log(error);
        }
    }, []);

    const toggleComplete = (todoToUpdate: Todo) => {
        try {
            updateNote({
                ...todoToUpdate,
                complete: !todoToUpdate.complete,
            })?.then((updatedTodo) =>
                setTodos(
                    todos.map((todo) =>
                        todo.id === updatedTodo.id ? updatedTodo : todo
                    )
                )
            );
        } catch (error) {
            console.log(error);
        }
    };

    const handleRemove = (id: number) => {
        try {
            deleteNote(id);
            setTodos(todos.filter((todo) => todo.id !== id));
        } catch (error) {
            console.log(error);
        }
    };

    const editTodo = (todoToUpdate: Todo) => {
        try {
            updateNote(todoToUpdate)?.then((updatedTodo) =>
                setTodos(
                    todos.map((todo) =>
                        todo.id === updatedTodo.id ? updatedTodo : todo
                    )
                )
            );
        } catch (error) {
            console.log(error);
        }
    };

    const addTodo = (text: string) => {
        try {
            addNote({ text, complete: false })?.then((todo) =>
                setTodos([...todos, todo])
            );
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <div className="App">
            <div className="new-note-form">
                <InputForm addTodo={addTodo} />
            </div>
            <div>
                <Search
                    searchQuery={searchQuery}
                    handleSearchQueryChange={(query) => setSearchQuery(query)}
                />
            </div>
            {todos.map(
                (todo) =>
                    todo.text
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase()) && (
                        <TodoNote
                            todo={todo}
                            toggleComplete={toggleComplete}
                            handleRemove={handleRemove}
                            key={randomId()}
                            editTodo={editTodo}
                        />
                    )
            )}
        </div>
    );
}

export default App;
