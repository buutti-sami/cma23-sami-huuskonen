import { Todo } from '../App';

const url = 'http://localhost:3001/todos';

export const getNotes = () => {
    try {
        const request = fetch(url);
        return request.then((response) => response.json());
    } catch (error) {
        console.log(error);
    }
};

export const addNote = (todo: { text: string; complete: boolean }) => {
    try {
        const request = fetch(url, {
            method: 'POST',
            body: JSON.stringify(todo),
            headers: {
                'Content-Type': 'application/json',
            },
        });
        return request.then((response) => response.json());
    } catch (error) {
        console.log(error);
    }
};

export const updateNote = (todo: Todo) => {
    try {
        const request = fetch(`${url}/${todo.id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(todo),
        });
        return request.then((response) => response.json());
    } catch (error) {
        console.log(error);
    }
};

export const deleteNote = (id: number) => {
    try {
        const request = fetch(`${url}/${id}`, {
            method: 'DELETE',
        });
        return request.then((response) => response.json());
    } catch (error) {
        console.log(error);
    }
};
