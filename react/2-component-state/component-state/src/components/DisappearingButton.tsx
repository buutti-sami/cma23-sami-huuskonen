import React from "react";

export default function DisappearingButton({
  number,
  hideNumber,
}: {
  number: number;
  hideNumber: (number: number) => void;
}) {
  return (
    <div>
      <button onClick={() => hideNumber(number)}>{`Button ${number}`}</button>
    </div>
  );
}
