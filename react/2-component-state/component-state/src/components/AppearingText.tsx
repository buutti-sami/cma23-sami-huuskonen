import React, { useState } from 'react';

export default function AppearingText() {
    const [showText, setShowText] = useState<boolean>(false);
    const text = 'Pöööööööööööööööö!';

    return (
        <div>
            <button onClick={() => setShowText((prevState) => !prevState)}>
                Toggle text
            </button>
            <p>{showText && text}</p>
        </div>
    );
}
