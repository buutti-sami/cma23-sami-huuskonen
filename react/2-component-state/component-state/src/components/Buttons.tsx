import React, { useState } from 'react';
import DisappearingButton from './DisappearingButton';

interface ShowButtons {
    [key: number]: boolean;
}

export default function Buttons() {
    const numberOfButtons = 5;
    const [showButtons, setShowButtons] = useState<ShowButtons>(() => {
        const initialButtons: ShowButtons = {};
        for (let i = 1; i <= numberOfButtons; i++) {
            initialButtons[i] = true;
        }

        return initialButtons;
    });

    const hideNumber = (buttonNumber: number) => {
        const updatedShowButtons = { ...showButtons };
        updatedShowButtons[buttonNumber] = false;
        setShowButtons(updatedShowButtons);
    };

    return (
        <div>
            {Object.entries(showButtons).map(([number, isVisible]) => (
                <div key={number}>
                    {isVisible && (
                        <DisappearingButton
                            hideNumber={hideNumber}
                            number={Number(number)}
                        />
                    )}
                </div>
            ))}
        </div>
    );
}
