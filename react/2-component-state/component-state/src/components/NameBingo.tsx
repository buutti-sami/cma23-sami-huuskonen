import React, { useState, useEffect } from 'react';

interface Props {
    names: string[];
}

interface NameBoxColors {
    [key: string]: boolean;
}

export default function NameBingo({ names }: Props) {
    const [namesChecked, setNamesChecked] = useState<NameBoxColors>(() => {
        const initialNamesChecked: NameBoxColors = {};
        for (let i = 0; i < names.length; i++) {
            initialNamesChecked[names[i]] = false;
        }
        return initialNamesChecked;
    });

    const [bingo, setBingo] = useState<boolean>(false);

    useEffect(() => {
        setBingo(checkBingo());
    }, [names, namesChecked]);

    const checkBingo = () => {
        const numOfRowsAndCols = Math.sqrt(names.length);

        const rowIndexesForBingo: number[][] = [];

        for (let i = 0; i < numOfRowsAndCols; i++) {
            const singleRowIndexes: number[] = [];

            for (let j = 0; j < numOfRowsAndCols; j++) {
                const bingoIndex = 1 + i * numOfRowsAndCols + j;
                singleRowIndexes.push(bingoIndex);
            }

            rowIndexesForBingo.push(singleRowIndexes);
        }

        const columnIndexesForBingo: number[][] = [];

        for (let i = 0; i < numOfRowsAndCols; i++) {
            const singleColumnIndexes: number[] = [];

            for (let j = 0; j < numOfRowsAndCols; j++) {
                singleColumnIndexes.push(j * numOfRowsAndCols + i);
            }
            columnIndexesForBingo.push(singleColumnIndexes);
        }

        const diagonalIndexesForBingo1 = [];
        for (let i = 1; i <= names.length; i += 6) {
            diagonalIndexesForBingo1.push(i);
        }

        const diagonalIndexesForBingo2 = [];
        for (let i = names.length - 5; i > 0; i -= 5) {
            diagonalIndexesForBingo2.push(i);
        }

        if (
            diagonalIndexesForBingo1.every(
                (i) => namesChecked[Object.keys(namesChecked)[i]]
            ) ||
            diagonalIndexesForBingo2.every(
                (i) => namesChecked[Object.keys(namesChecked)[i]]
            )
        ) {
            return true;
        }

        return false;
    };

    const toggleName = (name: string) => {
        const updatedNamesChecked = { ...namesChecked };
        updatedNamesChecked[name] = true;
        setNamesChecked(updatedNamesChecked);
    };

    const boxBgStyle = (isChecked: boolean) =>
        isChecked ? 'rgb(44, 248, 112)' : 'rgb(248, 75, 44)';

    return (
        <>
            {bingo && <p>Bingo!</p>}
            <div className="bingo-container">
                {Object.entries(namesChecked).map(([name, isChecked], i) => (
                    <div
                        key={name}
                        className="bingo-box"
                        style={{ backgroundColor: boxBgStyle(isChecked) }}
                        onClick={() => toggleName(name)}
                    >
                        {name}
                    </div>
                ))}
            </div>
        </>
    );
}
