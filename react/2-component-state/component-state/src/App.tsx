import React, { useState } from 'react';
import './App.css';
import AppearingText from './components/AppearingText';
import Buttons from './components/Buttons';
import NameBingo from './components/NameBingo';

const names = [
    'Anakin Skywalker',
    'Leia Organa',
    'Han Solo',
    'C-3PO',
    'R2-D2',
    'Darth Vader',
    'Obi-Wan Kenobi',
    'Yoda',
    'Palpatine',
    'Boba Fett',
    'Lando Calrissian',
    'Jabba the Hutt',
    'Mace Windu',
    'Padmé Amidala',
    'Count Dooku',
    'Qui-Gon Jinn',
    'Aayla Secura',
    'Ahsoka Tano',
    'Ki-Adi-Mundi',
    'Luminara Unduli',
    'Plo Koon',
    'Kit Fisto',
    'Shmi Skywalker',
    'Beru Whitesun',
    'Owen Lars',
];

function App() {
    return (
        <div className="App">
            <h1>Assignment 1</h1>
            <AppearingText />

            <h2>Assignment 2</h2>
            <Buttons />

            <h2>Assignment 3</h2>
            <NameBingo names={names} />
        </div>
    );
}

export default App;
