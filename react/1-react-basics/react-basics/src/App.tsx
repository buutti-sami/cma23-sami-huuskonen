import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Greeting from './components/Greeting';
import Planets from './components/Planets';
import BookListService from './components/BookListService';

export interface Book {
    name: string;
    pageCount: number;
}

const baseBooks: Book[] = [
    { name: 'Dune', pageCount: 412 },
    { name: 'The Eye of the World', pageCount: 782 },
];

function App() {
    const [books, setBooks] = useState<Book[]>(baseBooks);
    const fullName = 'Sami Harrisi';
    const age = 77;

    const planetList = [
        { name: 'Hoth', climate: 'Ice' },
        { name: 'Tattooine', climate: 'Desert' },
        { name: 'Alderaan', climate: 'Temperate' },
        { name: 'Mustafar', climate: 'Volcanic' },
    ];

    const addBook = (name: string, pageCount: string) => {
        setBooks([...books, { name, pageCount: Number(pageCount) }]);
    };

    return (
        <div className="App">
            <h1>Assignment 1</h1>
            <Greeting fullName={fullName} age={age} />

            <br />
            <h1>Assignment 2</h1>
            <div className="image-container">
                <img
                    src="https://gitea.buutti.com/education/academy-assignments/raw/branch/master/React/1.%20React%20Basics/r2d2.jpg"
                    alt="R2D2"
                />
                <p className="fat-text">Hello, I am R2D2!</p>
                <p className="italic-text">BeeYoop BedeepWeoop DeeepaEEye</p>
            </div>

            <br />
            <h1>Assignment 3</h1>
            <Planets planetList={planetList} />

            <br />
            <h1>Assignment 4</h1>
            <BookListService books={books} addBook={addBook} />
            <br />
        </div>
    );
}

export default App;
