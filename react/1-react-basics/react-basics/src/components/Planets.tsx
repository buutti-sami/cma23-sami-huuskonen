import React from 'react';

interface Planet {
    name: string;
    climate: string;
}

interface Props {
    planetList: Planet[];
}

export default function Planets({ planetList }: Props) {
    const tableHeaders = Object.keys(planetList[0]).map((col) => (
        <th key={col}>{col.toUpperCase()}</th>
    ));

    const tableContents = planetList.map((planet) => (
        <tr key={planet.name}>
            <td>{planet.name}</td>
            <td>{planet.climate}</td>
        </tr>
    ));

    return (
        <table>
            <thead>
                <tr>{tableHeaders}</tr>
            </thead>
            <tbody>{tableContents}</tbody>
        </table>
    );
}
