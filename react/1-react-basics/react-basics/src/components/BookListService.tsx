import React, { useState, FormEvent } from 'react';
import { Book } from '../App';
import { Button, Col, Container, Form, InputGroup, Row } from 'react-bootstrap';

interface Props {
    books: Book[];
    addBook: (name: string, pageCount: string) => void;
}
export default function BookListService({ books, addBook }: Props) {
    const [bookNameInputValue, setBookNameInputValue] = useState<string>('');
    const [PageCountInputValue, setPageCountInputValue] = useState<string>('');

    const renderedBooks = books.map((book) => (
        <Row key={book.name}>
            <Col>{book.name}</Col>
            <Col>{`(${book.pageCount} pages)`}</Col>
            <hr />
        </Row>
    ));

    const handleBookSubmit = (e: FormEvent) => {
        e.preventDefault();

        addBook(bookNameInputValue, PageCountInputValue);
        setBookNameInputValue('');
        setPageCountInputValue('');
    };

    return (
        <div className="book-list-container">
            <h2>BookListService</h2>
            <p>Books:</p>
            <Container>{renderedBooks}</Container>
            <p>Add new book:</p>
            <Form onSubmit={handleBookSubmit}>
                <Form.Group className="mb-3" controlId="formBookName">
                    <Form.Control
                        type="text"
                        placeholder="Book Name"
                        value={bookNameInputValue}
                        onChange={(e) => setBookNameInputValue(e.target.value)}
                    />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formPageCount">
                    <Form.Control
                        type="text"
                        placeholder="Page Count"
                        value={PageCountInputValue}
                        onChange={(e) => setPageCountInputValue(e.target.value)}
                    />
                </Form.Group>
                <Button variant="primary" type="submit">
                    Submit!
                </Button>
            </Form>
        </div>
    );
}
