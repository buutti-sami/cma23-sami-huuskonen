import React from 'react'

interface Props {
    fullName: string;
    age: number;
}

export default function Greeting({fullName, age}: Props) {
  return (
    <div>{`My name is ${fullName} and I am ${age}`} years old!</div>
  )
}
