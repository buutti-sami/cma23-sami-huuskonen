import React from 'react';
import styles from '../Search.module.css';

interface Props {
    searchQuery: string;
    handleSearchQueryChange: (query: string) => void;
}

export default function Search({
    searchQuery,
    handleSearchQueryChange,
}: Props) {
    return (
        <input
            type="text"
            value={searchQuery}
            onChange={(e) => handleSearchQueryChange(e.target.value)}
            placeholder="Search"
            name="searchQuery"
            id="searchQuery"
            className={styles.searchInput}
        />
    );
}
