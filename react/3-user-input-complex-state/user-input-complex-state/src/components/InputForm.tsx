import React, { FormEvent, useState } from 'react';
import styles from '../InputForm.module.css';

interface Props {
    addTodo: (text: string) => void;
}

export default function InputForm({ addTodo }: Props) {
    const [textInputValue, setTextInputValue] = useState<string>('');

    const handleSubmit = (e: FormEvent) => {
        e.preventDefault();
        addTodo(textInputValue);
        setTextInputValue('');
    };

    return (
        <form onSubmit={handleSubmit} id="addNewNoteForm">
            <input
                className={styles.newNoteInput}
                type="text"
                placeholder="Add a new note"
                onChange={(e) => setTextInputValue(e.target.value)}
                value={textInputValue}
                name="newNote"
                id="newNoteInput"
            />
            <button type="submit" name="submitButton" id="submitButton">
                Submit
            </button>
        </form>
    );
}
