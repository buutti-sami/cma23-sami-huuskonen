import React, { useState } from 'react';
import './App.css';
import TodoNote from './components/TodoNote';
import InputForm from './components/InputForm';
import Search from './components/Search';

export interface Todo {
    id: number;
    text: string;
    complete: boolean;
}

const defaultTodos: Todo[] = [
    { id: 1, text: 'Buy potatoes', complete: false },
    { id: 2, text: 'Make food', complete: false },
    { id: 3, text: 'Exercise', complete: false },
    { id: 4, text: 'Do the dishes', complete: false },
    { id: 5, text: 'Floss the teeth', complete: false },
    { id: 6, text: 'Play videogames', complete: true },
];

export const randomId = () => Math.random() * 100000;

function App() {
    const [todos, setTodos] = useState<Todo[]>(defaultTodos);
    const [searchQuery, setSearchQuery] = useState<string>('');

    const toggleComplete = (id: number) => {
        const updatedTodos = todos.map((todo) => {
            return todo.id === id
                ? { ...todo, complete: !todo.complete }
                : todo;
        });

        setTodos(updatedTodos);
    };

    const handleRemove = (id: number) => {
        const updatedTodos = todos.filter((todo) => todo.id !== id);
        setTodos(updatedTodos);
    };

    const editTodo = (id: number, text: string) => {
        const updatedTodos = todos.map((todo) => {
            return todo.id === id ? { ...todo, text } : todo;
        });

        setTodos(updatedTodos);
    };

    const addTodo = (text: string) => {
        setTodos([...todos, { text, complete: false, id: randomId() }]);
    };

    return (
        <div className="App">
            <div className="new-note-form">
                <InputForm addTodo={addTodo} />
            </div>
            <div>
                <Search
                    searchQuery={searchQuery}
                    handleSearchQueryChange={(query) => setSearchQuery(query)}
                />
            </div>
            {todos.map(
                (todo) =>
                    todo.text
                        .toLowerCase()
                        .includes(searchQuery.toLowerCase()) && (
                        <TodoNote
                            todo={todo}
                            toggleComplete={toggleComplete}
                            handleRemove={handleRemove}
                            key={randomId()}
                            editTodo={editTodo}
                        />
                    )
            )}
        </div>
    );
}

export default App;
