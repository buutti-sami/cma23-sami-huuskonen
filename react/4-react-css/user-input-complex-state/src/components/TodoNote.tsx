import React, { useState } from 'react';
import { Todo } from '../App';
import styles from '../TodoNote.module.css';

interface Props {
    todo: Todo;
    handleRemove: (id: number) => void;
    toggleComplete: (id: number) => void;
    editTodo: (id: number, text: string) => void;
}

export default function TodoNote({
    todo,
    handleRemove,
    toggleComplete,
    editTodo,
}: Props) {
    const [editMode, setEditMode] = useState<boolean>(false);
    const [todoTextInputValue, setTodoTextInputValue] = useState<string>(
        todo.text
    );

    const bgStyle = todo.complete ? '#00ff15' : '#ff3cdf';

    const handleSave = (id: number) => {
        editTodo(id, todoTextInputValue);
        setEditMode(false);
    };
    const editModeOffContent = () => (
        <>
            <p>{todo.text}</p>{' '}
            <button onClick={() => setEditMode(true)}>Edit</button>
        </>
    );

    const editModeOnContent = (todo: Todo) => (
        <>
            <input
                type="text"
                onChange={(e) => setTodoTextInputValue(e.target.value)}
                value={todoTextInputValue}
            ></input>
            <button onClick={() => handleSave(todo.id)}>Save</button>
        </>
    );

    return (
        <div className={styles.todoNote} style={{ backgroundColor: bgStyle }}>
            <label>
                <input
                    type="checkbox"
                    checked={todo.complete}
                    onChange={() => toggleComplete(todo.id)}
                />
                Completed
            </label>
            {editMode ? editModeOnContent(todo) : editModeOffContent()}
            <div
                onClick={() => handleRemove(todo.id)}
                className={styles.removeBtn}
            >
                X
            </div>
        </div>
    );
}
