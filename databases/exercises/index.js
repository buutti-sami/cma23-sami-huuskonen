import express from 'express';
import dotenv from 'dotenv';
import { createProductsTable } from './dao.js';
import productRouter from './routes/productRouter.js';

dotenv.config();

const server = express();

server.use(express.json());

const PORT = process.env.PORT || 3003;

createProductsTable();

server.use('/', productRouter);

server.listen(PORT, () => {
  console.log(`server listening on port: `, PORT);
});
