const createProductsTableQuery = `
CREATE TABLE IF NOT EXISTS "products" (
    "id" SERIAL PRIMARY KEY,
    "name" VARCHAR(100) NOT NULL,
    "price" REAL NOT NULL
)`;

const insertProductQuery = `
INSERT INTO products (name, price)
VALUES ($1, $2) RETURNING id
`;

const getAllProductsQuery = `
SELECT * FROM products
`;

const getProductByIdQuery = `
SELECT * FROM products WHERE id = $1
`;

const updateProductByIdQuery = `
UPDATE products
SET name = $2, price = $3
WHERE id = $1
`;

const deleteProductByIdQuery = `
DELETE FROM products WHERE id = $1
`;

export {
  createProductsTableQuery,
  insertProductQuery,
  getAllProductsQuery,
  getProductByIdQuery,
  updateProductByIdQuery,
  deleteProductByIdQuery,
};
