import { executeQuery } from './db.js';
import {
  createProductsTableQuery,
  deleteProductByIdQuery,
  getAllProductsQuery,
  getProductByIdQuery,
  insertProductQuery,
  updateProductByIdQuery,
} from './queries.js';

export const createProductsTable = async () => {
  await executeQuery(createProductsTableQuery);
  console.log('Products table initialized');
};

export const insertProduct = async (product) => {
  const params = [...Object.values(product)];
  console.log(`Inserting a new product ${params[0]}...`);
  try {
    const result = await executeQuery(insertProductQuery, params);
    console.log(`Product ${product.name} inserted successfully`);
    return result;
  } catch (error) {
    throw new Error(error);
  }
};

export const getAllProducts = async () => {
  console.log('Getting all products...');
  try {
    const result = await executeQuery(getAllProductsQuery);
    console.log('Products fetched successfully');
    return result;
  } catch (error) {
    throw new Error(error);
  }
};

export const getProductById = async (id) => {
  const params = [id];
  console.log('Getting a product by id of ' + id);
  try {
    const result = await executeQuery(getProductByIdQuery, params);
    console.log(`Product ${id} fetched successfully`);
    return result;
  } catch (error) {
    throw new Error(error);
  }
};

export const updateProductById = async (id, name, price) => {
  const params = [id, name, price];
  console.log('Updating a product ' + id);
  try {
    const result = await executeQuery(updateProductByIdQuery, params);
    console.log(`Product ${id} updated successfully`);
    return result;
  } catch (error) {
    throw new Error(error);
  }
};

export const deleteProductById = async (id) => {
  const params = [id];
  console.log(`Deleting product ${id}`);

  try {
    const result = await executeQuery(deleteProductByIdQuery, params);
    console.log(`Product ${id} deleted succcessfully`);
    return result;
  } catch (error) {
    throw new Error(error);
  }
};
