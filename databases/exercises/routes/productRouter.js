import { Router } from 'express';
import {
  deleteProductById,
  getAllProducts,
  getProductById,
  insertProduct,
  updateProductById,
} from '../dao.js';

const router = Router();

const handleError = (res, error) => {
  console.log(error);
  res.status(400).send(error);
};

const validateProduct = (product) => {
  return product && product.name && product.price;
};

const sendSuccess = (res, data) => {
  res.status(200).json(data);
};

router.post('/', async (req, res) => {
  const product = req.body;

  if (!validateProduct(product)) {
    return res.status(400).send('valid request body missing');
  }

  try {
    const result = await insertProduct(product);
    const insertedProduct = { id: result.rows[0], ...product };
    sendSuccess(res, insertedProduct);
  } catch (error) {
    handleError(res, error);
  }
});

router.get('/', async (_req, res) => {
  try {
    const result = await getAllProducts();
    if (result) {
      const products = result.rows;
      sendSuccess(res, products);
    }
  } catch (error) {
    handleError(res, error);
  }
});

router.get('/:id', async (req, res) => {
  const { id } = req.params;

  try {
    const result = await getProductById(id);
    if (result) {
      const product = result.rows;
      sendSuccess(res, product);
    }
  } catch (error) {
    handleError(res, error);
  }
});

router.put('/:id', async (req, res) => {
  const { id } = req.params;
  const { name, price } = req.body;

  if (!validateProduct({ name, price })) {
    return res.status(400).send('Invalid request body');
  }

  try {
    const result = await updateProductById(id, name, price);
    if (result) {
      sendSuccess(res);
    }
  } catch (error) {
    handleError(res, error);
  }
});

router.delete('/:id', async (req, res) => {
  const { id } = req.params;

  if (!id) {
    return res.status(400).send('Req param id missing');
  }

  try {
    const result = await deleteProductById(id);
    if (result) {
      sendSuccess(res);
    }
  } catch (error) {
    handleError(res, error);
  }
});

export default router;
